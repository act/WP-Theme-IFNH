jQuery(window).load(function () {


    uorlogo();

    if (jQuery('.footer-area-middle .menu').length > 0) {
        var Count = jQuery('.footer-area-middle .menu li').length;
        jQuery('.footer-area-middle .menu li').slice(Math.floor(Count / 2), Count).addClass('SecondHalf');
    }

    jQuery('.menu-footer-one-container').append('<ul id="menu-footer-spl" class="menu"></ul>');

    jQuery('.footer-area-middle .menu li.SecondHalf').each(function () {
        jQuery('#menu-footer-spl').append(jQuery(this).clone());
    });

    //jQuery('.footer-area-middle .Thirdcol').children().wrapAll('<div class="widget widget_nav_menu splnavfooter"><ul class="menu splmenu"></ul></div>');
    jQuery('#menu-footer-one li.SecondHalf').remove();

    jQuery('.menu-footer-bottom-container').prepend(jQuery('.cpfooter'));

    HeroesBG();
    //ThemeshomeHtAdjust();

    CasesHtAdjust();
    NewsHtAdjust();


    //Primary Interest filtering starts 
    // Members filter by Primary ineterest
        var currfilter = "";
    
        jQuery('.IFNHMemFilter li').click(function()
        {    
            jQuery('.IFNHMemFilter li').removeClass('memfilter-current');
            jQuery(this).addClass('memfilter-current');
    
            currfilter = jQuery(this).attr("id").replace('FilterOption-','');
            //currfilter = jQuery(this).attr("data-filter");
    
            if(currfilter != "")
            {

                jQuery("#MembersListing .stafflistcol .at-person  > .MemOuter > div").css("opacity", "1").fadeIn('slow');	                
                jQuery("#MembersListing .stafflistcol .at-person  > .MemOuter > div:not(." + currfilter+ ")").css("opacity", "0").hide();
            }
    
            else
            {			
                jQuery("#MembersListing .stafflistcol .at-person  > .MemOuter > div").css("opacity", "1").fadeIn('slow');
            }
    
    
        });



        //Sorting alphabetically by Primary ineterest

        var MemUL= jQuery("#MembersListing .stafflistcol .at-person > .MemOuter");  
        var MemList = jQuery("#MembersListing .stafflistcol .at-person > .MemOuter > div");    

        MemList.sort(function (a, b) {
            var classA = jQuery(a).attr("class");
            var classB = jQuery(b).attr("class");
            if (classA > classB) return 1;
            if (classA < classB) return -1;
            return 0;
        }).appendTo(MemUL);


        //grouping by Primary ineterest and appending heading
        var classes = {};
        jQuery("#MembersListing .stafflistcol .at-person > .MemOuter > div").each(function() {
            var temp = jQuery(this).attr("class");
            if (temp) {
                classes[temp] = true;
            }
        });
        for (singleClass in classes) {
            jQuery("." + singleClass).wrapAll('<div class="PrimInterestroup ' + singleClass + "-Group" + '" />');
        }
        

        jQuery(".PrimInterestroup").each(function() {
            var InterestHeading = jQuery(this).find('div:first-of-type').attr("class").replace(/\-/g, ' ');
            if (InterestHeading) {
                jQuery(this).prepend("<h3>"+ InterestHeading+ "</h3>");
            }
        });


        //Count text
        var counttext = "";
        var individelem = "";
        var i=0;
        jQuery(".IFNHMemFilter .interest-filter .count").each(function() {
            //individelem = jQuery(this).closest('.interest-filter').attr("data-filter").replace("-Group", "");
            individelem = jQuery(this).closest('.interest-filter').attr("id").replace('FilterOption-','').replace("-Group", "");

            if(individelem != "")
            {
                counttext = jQuery(".PrimInterestroup  ." + individelem).length;
                jQuery(this).text("(" + counttext + ")");    
                
				if(counttext == 0)
				{
					jQuery(this).closest('.interest-filter').hide();
				}
				
                i= i+counttext;
            }
           
        });

        
        jQuery('.allcount').text("(" +jQuery('.at-person-content').length + ")"); 

    //Primary Interest filtering ends 
	
	///// For resource pages - begins
	


	//Setting Active class on Resource page
	var currurl = urldecode(document.location.href);
	if (currurl.indexOf("contentforid") > 0) {
		 var idToBeActive = "#contentforid-"+ getUrlParameter("contentforid");
         
       
		 jQuery(idToBeActive).parent().find('a').removeClass('active');

		 //if(!jQuery(idToBeActive).hasClass('active'))		 
			jQuery(idToBeActive).addClass('active');
		jQuery('html, body').animate({scrollTop:jQuery(idToBeActive).position().top}, 'slow');
		
	}
	///// For resource pages - ends
	
	//making username/email readonly on edit-profile page
    jQuery(".memregform.editprofile .readonly input").attr('readonly','readonly').attr('disabled','disabled');

});


jQuery(window).resize(function () {
    uorlogo();
    HeroesBG();
    //ThemeshomeHtAdjust();
    CasesHtAdjust();
    NewsHtAdjust();
});

function uorlogo() {
    if (jQuery('.iva-mobile-menu .UoRLogo').length <= 0) {
        var uorhtml = jQuery('.UoRLogo').clone();
        jQuery('.iva-mobile-menu').append(uorhtml);
    }
}



//Home page theme boxes height adjustment on hover
function ThemeshomeHtAdjust() {
    setTimeout(function () {

        var max = -1;
        jQuery(".themeshome .vc_grid-item").each(function () {
            var h = jQuery(this).find('.title').height();
            max = h > max ? h : max;
        });
        max = max + 20;
        jQuery(".vc_gitem-row-position-bottom").css("height", max);

        if (jQuery(window).width() > 960) {
            jQuery(".themeshome .vc_grid-item").each(function () {
                var hH5 = jQuery(this).find('.vc_gitem-row-position-bottom .vc_custom_heading ').outerHeight();
                var hdesc = jQuery(this).find('.themedesc').outerHeight() + 78;
                var htotal = hH5 + hdesc;

                jQuery(this).mouseenter(function () {
                    jQuery(this).find('.vc_gitem-row-position-bottom').css("height", htotal + "px");
                })
              .mouseleave(function () {
                  jQuery(this).find(".vc_gitem-row-position-bottom").css("height", max);
              });

            });
        }

    }, 2000);
}


function CasesHtAdjust() {
    setTimeout(function () {

        var max = -1;
        jQuery(".at-cases-item").each(function () {
            var h = jQuery(this).find('.at-cases-content span.title').height();
            max = h > max ? h : max;
        });
        max = max + 20;
        jQuery(".at-cases-main .at-cases-details .at-cases-content").css("height", max);

        if (jQuery(window).width() > 960) {
            jQuery(".at-cases-item").each(function () {
                var descH = jQuery(this).find('.cusdesc').innerHeight();

                var totalH = max + descH + 40;

                jQuery(this).mouseenter(function () {
                    //  alert(totalH);
                    jQuery(this).find('.at-cases-content').css("height", totalH + "px");
                })
              .mouseleave(function () {
                  jQuery(this).find('.at-cases-content').css("height", max);
              });
            });
        }

    }, 1000);

}

function NewsHtAdjust() {
    setTimeout(function () {
        var max = -1;
        jQuery(".FeaturedNewsRow .post").each(function () {
            var h = jQuery(this).find('.at-post-content').height();
            max = h > max ? h : max;
        });
        max = max + 50;

        jQuery(".FeaturedNewsRow .post .at-post-content").css("height", max);

    }, 1000);

}

function HeroesBG() {
    setTimeout(function () {
        var imgsrc = "";
        jQuery(".flexslider ul li").each(function () {

            imgsrc = jQuery(this).find('img').attr("src");
            // alert(imgsrc);

            if (imgsrc != undefined)
            // jQuery(this).find('.bgdiv').css("background-image", imgsrc);

                jQuery(this).css("background-image", 'url(' + imgsrc + ')');
        });
    }, 2000);
}

//This function is for grouping with year on Publications
var year1 = jQuery('.page-template-publications .pubdetails .publist:first-of-type h2.pubyear').text().trim();
var i = 0;
jQuery('.page-template-publications .pubdetails .publist').each(function () {

    if (jQuery(this).find('h2.pubyear').text().trim() == year1) {
        if (i > 0) {
            jQuery(this).find('h2.pubyear').hide();
        }
    }
    else {
        i = 0;
        year1 = jQuery(this).find('h2.pubyear').text().trim();
    }

    i++;
});

//used on resource page

//This function is for grouping with groupname in Resource pages
	// Get all the classes


var classes = jQuery('[class*= "ResourceItemInner"]').map(function() {
	return jQuery(this).attr('class');
});
// Filter only unique ones
var uniqueClasses = jQuery.uniqueSort(classes);

// Now grouping them
jQuery(uniqueClasses).each(function(i, v)
{
	jQuery('.'+v).wrapAll('<div class ="parent-'+v+'"></div>');
});


function urldecode(str) {
    return decodeURIComponent((str + '').replace(/\+/g, '%20'));
}

function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');


        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1].replace("%20", " ");
        }
    }
}



