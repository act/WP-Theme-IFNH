<?php
require($_SERVER['DOCUMENT_ROOT'].'/wp-load.php');
session_start();


$memberUserName = $_POST["memberUserName"];
$pwd = $_POST["pwd"];


$email = "";


/*if (!str_starts_with(getenv("HTTP_REFERER"), home_url() . "/member-login/"))
{
	header( "Location: /member-login/");	
	exit();
}*/

// check for reminder first
$FP = $_POST["fp"];

$memberEmail = $_POST["memberEmail"];



if (!empty($FP) && !empty($memberEmail))
{
	// okay, looks like a reminder
	SendNewPassword( $memberEmail);
	header( "Location: /ifnh/member-login/?e=remind");	
	exit();
}


if (empty($memberUserName))
{
	header( "Location: /ifnh/member-login/?e=err2");	
	exit();
}





// next validate against the website. Currently this is a separate table containing: MemberNo, Email, Pwd, Created, LastLogin

// query local table for memberno / pwd combination
$auth = authUser($memberUserName, $pwd);


if ($auth == "1") {
	// update last login and log them in
	setcookie("loggedin_user", 1, time()+7200, '/');
	//setcookie("loggedin_username", $memberUserName, time()+7200, '/');
	//setcookie("loggedin_userno", wp_hash_password($memberNo), time()+7200, '/');
	//setcookie("loggedin_userno", $memberNo, time()+7200, '/');
	session_start();
	$_SESSION["memberUserName"] = $memberUserName;
	
	header( "Location: /ifnh/home/" );
}
else {
	setcookie("loggedin_user", 0, time()+7200, '/');
	header( "Location: /ifnh/member-login?e=" . $auth );
}


function authUser($memberUserName, $pwd)
{
	$returnvalue = '';
	
	$query = array(
		'post_type' => 'staff',
		'meta_query' => array(
        array(
            'key'     => 'member_username',
            'value'   => trim($memberUserName)          
        )
        )
	);

	//$wp_hasher = new PasswordHash(8, TRUE);

		
	$member_query = new WP_Query( $query );
	//echo $member_posts->request;
	$member_posts = $member_query->posts;
	

	
	
	// not found
	if($member_query->post_count <= 0) 
	{
		// if there's an email address, but no account exists with the username. 
		if ($memberUserName != '')
		{
			$returnvalue = "LOGIN_ERROR_NEW_001&UN=". $memberUserName;
		}
		else
		{
			$returnvalue = "LOGIN_ERROR_NEW_002";
		}
		
	}
	else 
	{
		
		foreach ( $member_posts as $member ) {			

			
			if (wp_check_password($pwd, get_field('member_password', $member -> ID))) {
				//echo "YES, Matched";
								
				
				update_post_meta( $member->ID, 'last_login', date('Y-m-d H:i:s') );
				$returnvalue = "1";	
				
			} else {
				//echo "No, Wrong Password";
				$returnvalue = "err";
				
			}

		}
	}
		
	return $returnvalue;
}

function str_starts_with($haystack, $needle)
{
    return substr_compare($haystack, $needle, 0, strlen($needle)) === 0;
}

/*function CreateAndEmail()
{
	$user_id = get_current_user_id();
	$postarr = array(
		'post_author' => $user_id,
		'post_title' => $GLOBALS['memberUserName'],
		'post_type' => 'staff',
		'comment_status' => 'closed',
		'ping_status' => 'closed'
	);
	$postid = wp_insert_post( $postarr, false ); 
	$newpass = wp_generate_password( 8, true );
	update_post_meta( $postid, 'labora_email', (string)$GLOBALS['memberUserName'] );
	update_post_meta( $postid, 'member_username', $GLOBALS['memberUserName'] );
	update_post_meta( $postid, 'member_password', wp_hash_password($newpass) ); // encrypt

	
	// send email	
	SendPasswordEmail((string)$GLOBALS['memberUserName'], $newpass);
}*/

function SendPasswordEmail($email, $pass)
{
   
	$subject = "IFNH - Login details";
	$html = file_get_contents('/wp-content/themes/IFNH//passwordemail.html', true);
	//$message = sprintf($html,$pass,home_url());
	
	//

    $message .= '<div id="wrapper"><table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%"><tbody><tr><td align="center" valign="top"><div id="template_header_image"><p style="margin-top:0;"><img src="https://research.reading.ac.uk/ifnh/wp-content/uploads/sites/19/2018/07/IFNH-logo-RGB.png" width="175" height="70" alt="IFNH"></p></div>';
    $message .= '<table border="0" cellpadding="0" cellspacing="0" width="600" id="template_container"><tbody><tr><td align="center" valign="top"><h1>New password</h1></td></tr><tr><td align="center" valign="top"><table border="0" cellpadding="0" cellspacing="0" width="600" id="template_body"><tbody><tr><td valign="top" align="center" id="body_content"><div id="body_content_inner">';
    $message .= 'Your new password is: '.$pass;
    $message .= '</div></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></div>';
    //
	
	// Always set content-type when sending HTML email
   
   	
   	$headers[] = 'MIME-Version: 1.0' . "\r\n";
    $headers[] = 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $headers[] = "X-Mailer: PHP \r\n";
    $headers[] = 'From: ifnh@omailer.com' . "\r\n";

	// More headers

	 //echo '$email: '.$email .'<br/>';
	 //echo '$subject: '.$subject.'<br/>';echo '$headers: '.$headers.'<br/>';
	 //echo '$message: '.$message.'<br/>';
	 
	

	wp_mail($email,$subject,$message,$headers);
}

function SendNewPassword( $memberEmail)
{
	
	$query = array(
		'post_type' => 'staff',
		'meta_query' => array(			
            array(
				'key'   => 'labora_email',
				'value' => $memberEmail,
				'compare' => '=',
			)
		)
		
	);
		
	$member_query  = new WP_Query( $query );
	$member_posts = $member_query->posts;

	if($member_query->post_count > 0) 
	{
		foreach ( $member_posts as $member ) {
			//echo "YES, Matched";
			
			$newpass = wp_generate_password( 8, true );
			update_post_meta( $member->ID, 'member_password', wp_hash_password($newpass));
			SendPasswordEmail($memberEmail, $newpass);
		}
	}
	
	else{
		
		header( "Location: /ifnh/member-login?e=LOGIN_ERROR_NEW_003");
		exit();
	}
		
}

/*
function isUserValid($memberNo, $memberUserName){

	//return "True";
	$rev_username = "MGCC";
	$rev_password = "Password1";

	$url = "http://revupws.prosolvehosting.co.uk/RevUpWebService.asmx/GetMemberDetails?username=".$rev_username."&password=".$rev_password."&memberNumber=".$memberNo."&authenticationValues=".$memberUserName;
	//$url = "http://mgccsite.oberginetest.co.uk/ws.txt";
	
	$xml = simplexml_load_file($url);
	// save this for later
	$GLOBALS['email'] = $xml->Email;

	//print_r($xml);
	//echo($GLOBALS['email']);
	//die('g');
		
	return $xml->IsValid;
}
*/
?>