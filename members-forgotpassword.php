<?php
/**
 * Template Name: Members forgot password
 */
?>

<?php
	//Includes the header.php template file from your current theme's directory
	get_header();
?>

<div id="primary" class="pagemid ForgotPWDivOuter">
	<div class="inner">
		<main class="content-area">

<div class="vc_row wpb_row vc_row-fluid FullWidthFormRow">

<div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-offset-2 vc_col-lg-8 vc_col-md-offset-0 vc_col-md-12 vc_col-sm-offset-0 vc_col-xs-12">
<div class="vc_column-inner">
<div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">

<?php
        global $wpdb;
        
        $error = '';
        $success = '';
        
        // check if we're in reset form
        if( isset( $_POST['action'] ) && 'reset' == $_POST['action'] ) 
        {
            $email = trim($_POST['user_login']);
            
            if( empty( $email ) ) {
                $error = 'Enter your e-mail address...';
            } else if( ! is_email( $email )) {
                $error = 'Invalid  e-mail address.';
            } else if( ! email_exists( $email ) ) {
                $error = 'There is no user registered with that email address.';
            } else {
                
                $random_password = wp_generate_password( 12, false );
                $user = get_user_by( 'email', $email );
                
                $update_user = wp_update_user( array (
                        'ID' => $user->ID, 
                        'user_pass' => $random_password
                    )
                );
                
                // if  update user return true then lets send user an email containing the new password
                if( $update_user ) {
                    $to = $email;
                    $subject = 'IFNH - Your new password';
                    $sender = get_option('name');
                    //

                    $message .= '<div id="wrapper"><table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%"><tbody><tr><td align="center" valign="top"><div id="template_header_image"><p style="margin-top:0;"><img src="https://research.reading.ac.uk/ifnh/wp-content/uploads/sites/19/2018/07/IFNH-logo-RGB.png" width="175" height="70" alt="IFNH"></p></div>';
                    $message .= '<table border="0" cellpadding="0" cellspacing="0" width="600" id="template_container"><tbody><tr><td align="center" valign="top"><h1>New password</h1></td></tr><tr><td align="center" valign="top"><table border="0" cellpadding="0" cellspacing="0" width="600" id="template_body"><tbody><tr><td valign="top" align="center" id="body_content"><div id="body_content_inner">';
                    $message .= 'Your new password is: '.$random_password .'<p>Please reset it after you login with this password.</p>';
                    $message .= '</div></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></div>';
                    //
                    
                    
                    $headers[] = 'MIME-Version: 1.0' . "\r\n";
                    $headers[] = 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                    $headers[] = "X-Mailer: PHP \r\n";
                    $headers[] = 'From: '.$sender.' < '.$email.'>' . "\r\n";
                    
                    $mail = wp_mail( $to, $subject, $message, $headers );
                    if( $mail )
                        $success = 'Check your email address for your new password.';
                        
                } else {
                    $error = 'Oops something went wrong updating your account.';
                }
                
            }
            
            if( ! empty( $error ) )
                echo '<span class="error FWLabel"><strong>ERROR:</strong> '. $error .'</span>';
            
            if( ! empty( $success ) )
                echo '<span class="success FWLabel">'. $success .'</span>';
        }
    ?>

 
		
			<form method="post">
				<fieldset>
					<p>Please enter your email address. You will receive a new password via email.</p>
					<p><label for="user_login">E-mail:</label>
						<?php $user_login = isset( $_POST['user_login'] ) ? $_POST['user_login'] : ''; ?>
						<input type="text" name="user_login" id="user_login" value="<?php echo $user_login; ?>" /></p>
					<p>
						<input type="hidden" name="action" value="reset" />
						<input type="submit" value="Get New Password" class="button btn" id="submit" />
					</p>
				</fieldset> 
			</form>
			
			
			
			
		 </div><!-- .entry-content-wrapper -->
		 </div> </div> </div> </div>
	
</div>
</main>
</div>
</div>


<style>

.ForgotPWDivOuter .vc_col-lg-offset-2
{
    margin-left: 16.67%;
    padding-top: 80px;
    padding-bottom: 80px;
    margin-right: 16.67%;
}

.ForgotPWDivOuter p,
.ForgotPWDivOuter label,
.ForgotPWDivOuter input[type="text"]
{
   display: block !important;
   width: 100%;
}

.ForgotPWDivOuter input[type="text"]
{
 background-color: #efefef;
border: none;
padding: 5px 10px;
height: 50px !important;
}



.ForgotPWDivOuter .error.FWLabel
{
    color:red;
}


</style>

<?php get_footer(); ?>