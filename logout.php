<?php /* Template Name: Member logout */ ?>
<?php
setcookie("loggedin_user", '', time()-3600, '/');
setcookie("loggedin_username", '', time()-3600, '/');
setcookie("loggedin_userno", '', time()-3600, '/');

// remove all session variables
session_start();
$_SESSION = array();

// destroy the session 
session_destroy(); 

header( "Location: /ifnh/" );
?>