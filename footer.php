<?php
/**
 * Footer Sidebar
 */
?>
</div><!-- #main -->
	<div id="footer">

		<?php
		// Footer Area Top
		if ( get_option( 'labora_footer_area_top' ) != 'on' ) {
			if ( is_active_sidebar( 'footer_area_top' ) ) {
				echo '<div class="footer-area-top"><div class="inner">';
				dynamic_sidebar( 'footer_area_top' );
				echo '</div></div>';
			}
		}
		if ( get_option( 'labora_footer_sidebar' ) != 'on' ) {
			// Get footer sidebar widgets
			if ( get_option( 'labora_footer_sidebar' ) != 'on'
				&& is_active_sidebar( 'footer-widgets' )
				) {
				echo '<div class="footer-area-middle">';
				echo '<div class="inner">';
				echo '<div class="inner-row">';
				if ( is_active_sidebar( 'footer-widgets' ) ) : dynamic_sidebar( 'footer-widgets' ); endif;
				echo '</div>';
				echo '</div>';
				echo '</div>';
			}
		}
		// Footer Area Bottom
		if ( get_option( 'labora_footer_area_bottom' ) != 'on' ) {
			if ( is_active_sidebar( 'footer_area_bottom' ) ) {
				echo '<div class="footer-area-bottom"><div class="inner">';
				dynamic_sidebar( 'footer_area_bottom' );
				echo '</div></div>';
			}
		} ?>

       <span class="cpfooter">Copyright &copy; <?php echo date('Y'); ?></span> 

		<?php if ( is_active_sidebar( 'footer_leftcopyright' ) || is_active_sidebar( 'footer_rightcopyright' ) ) {  ?>
			<div class="copyright">
				<div class="inner">
					<div class="copyright_left">
						<?php
						if ( is_active_sidebar( 'footer_leftcopyright' ) ) :
							dynamic_sidebar( 'footer_leftcopyright' );
						endif; ?>
					</div>
					<div class="copyright_right">
						<?php
						if ( is_active_sidebar( 'footer_rightcopyright' ) ) :
							dynamic_sidebar( 'footer_rightcopyright' );
						endif;
						if ( is_active_sidebar( 'footer-branches' ) ) {
							echo '<a class="at-footer-branches">' . esc_html__( 'More Branches', 'labora' ) . '</a>';
						}
						?>
					</div>

				</div><!-- .inner -->
			</div><!-- .copyright -->
		<?php } ?>
		<?php
		if ( is_active_sidebar( 'footer-branches' ) ) { ?>
			<div class="footer-branches">
				<div class="inner">
					<?php
					if ( is_active_sidebar( 'footer-branches' ) ) : dynamic_sidebar( 'footer-branches' );
					endif;
					?>
				</div>
			</div>
		<?php } ?>
	</div> <!-- .footer-area -->
</div><!-- #wrapper -->

<?php
$labora_headerstyle = get_option( 'labora_headerstyle' ) ? get_option( 'labora_headerstyle' ) : '';
if ( $labora_headerstyle === 'headerstyle4' ) { ?>
</div><!-- .sidebar-leftmenu -->
<?php } ?>

<div id="back-top">
	<a href="#header" title="<?php echo esc_html__( 'Scroll Top', 'labora' ); ?>"><span class="fa fa-angle-up fa-fw fa-2x"></span></a>
</div>
<?php wp_footer();?>

<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/contact.js"></script>


<script type="text/javascript" src="<?php bloginfo('stylesheet_directory');?>/js/site.js"></script>
</body>
</html>
<?php
