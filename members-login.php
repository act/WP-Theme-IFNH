<?php
/**
 * Template Name: Member login
 */
?>

<?php
	//Includes the header.php template file from your current theme's directory
	get_header();
?>

<div id="primary" class="pagemid ForgotPWDivOuter">
	<div class="inner">
		<main class="content-area">

<div class="vc_row wpb_row vc_row-fluid FullWidthFormRow">

<div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-offset-2 vc_col-lg-8 vc_col-md-offset-0 vc_col-md-12 vc_col-sm-offset-0 vc_col-xs-12">
<div class="vc_column-inner">
<div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper entry-content-wrapper">

    <?php

        $error = $_GET['e'];
        $msg = '';
        if ($error == 'err') { $msg = "<p class='error'>There was an error with your login details. Please try again.</p>";}
        else if ($error == 'remind') { echo "<p class='error'>Thank you. We have sent a new password to the email address on record.</p>"; }
        else if ($error == 'LOGIN_ERROR_NEW_001') {$msg = "<p class='error'>Sorry, we could not find a record with these details.</p>";} 
        else if ($error == 'LOGIN_ERROR_NEW_002') {$msg = "<p class='error'>Sorry, we could not find a record with these details.</p>";} 
        else if ($error == 'LOGIN_ERROR_NEW_003') {$msg = "<p class='error'>Sorry, we could not find a record with these details.</p>";} 
        if ($msg != '') {echo $msg;}
		
	?>
	
    <p class='error errormyForm' style="display:none;">Please enter your username and password.</p>
	
	
	<form id="myForm" action="/ifnh/wp-content/themes/IFNH/log_code.php" method="post">
		<p>
		<label for="memberUserName">Username:</label>
		<input type="text" name="memberUserName" id="memberUserName" value="" tabindex="1" />
		</p>
	
		
		<p id="pfield">
		<label for="pwd">Password:</label>
		<input type="password" name="pwd" id="pwd" value="" tabindex="3" autocomplete="off" />
		</p>
		<p><input type="submit" value="Submit" id="myFormSubmit" /></p>
		
	</form>
	<p>		<br /><a href="#fpForm" class="fp" onclick="jQuery('#fpForm').show();">Forgotten your password? Click here to reset</a>.
</p>
	
	<style>
	.error {color:red;}
	</style>

<p class='error errorfpForm' style="display:none;">Please enter your email.</p>
<p class='error errorinvalidemail' style="display:none;">Please enter a valid email address.</p>
	<form id="fpForm" style="display:none" action="/ifnh/wp-content/themes/IFNH/log_code.php" method="post">
		<h3>Forgotten password</h3>
	
		<p>
		<label for="name">Email:</label>
		<input type="text" name="memberEmail" id="memberEmail" value="" />
		</p>
		<p>
		<input type="hidden" name="fp" id="fp" value="true" />
		<input type="submit" value="Submit" id="fpFormSubmit"/>
		</p>
		
	</form>			
		 </div><!-- .entry-content-wrapper -->
		 </div> </div> </div> </div>
	
</div>
</main>
</div>
</div>

<script type="text/javascript">

jQuery(function()
{

    jQuery('#myFormSubmit').click(function(e)
    {
        if((jQuery('#memberUserName').val().length <=0) || (jQuery('#pwd').val().length <=0))
        {
            jQuery('.errormyForm').show();
            e.preventDefault();
        }
        else
        {
            jQuery('.errormyForm').hide();
        }
   })


   jQuery('#fpFormSubmit').click(function(e)
    {
    if(jQuery('#memberEmail').val().length <=0)
    {
        jQuery('.errorfpForm').show();    
        e.preventDefault();        
    }

    else if(!validateEmail(jQuery('#memberEmail').val()))
    {
        jQuery('.errorinvalidemail').show();
        e.preventDefault();       
    }

    else
    {
        jQuery('.errorfpForm').hide();
    }
})   

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}
})
    
</script>


<?php get_footer(); ?>