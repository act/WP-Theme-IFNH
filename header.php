<?php
/**
 * The template for displaying the header
 * @package WordPress
 * @subpackage Labora
 * @since Labora 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	
    <link rel="icon" href="/wp-content/themes/IFNH/images/favicons/favicon-32x32.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" href="/wp-content/themes/IFNH/images/favicons/apple-touch-icon-114x114.jpg" />
    <link rel="apple-touch-icon" sizes="72x72" href="/wp-content/themes/IFNH/images/favicons/apple-touch-icon-114x114.jpg" />
    <link rel="apple-touch-icon" sizes="114x114" href="/wp-content/themes/IFNH/images/favicons/apple-touch-icon-114x114.jpg" />


	<?php wp_head(); ?>

     <!-- Global Site Tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-18854554-13"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments)};
      gtag('js', new Date());

      gtag('config', 'UA-18854554-13');
    </script>

</head>

<body <?php body_class(); ?>>   


<?php
	// Pulls an frontpage id ID for page slider
if (
	is_tag() ||
	is_search() ||
	is_404() ||
	is_home()
) {
	$labora_frontpageid = '';
} else {
	if ( class_exists( 'woocommerce' ) ) {
		if ( is_shop() ) {
			$labora_frontpageid = get_option( 'woocommerce_shop_page_id' );
		} elseif ( is_cart( get_option( 'woocommerce_cart_page_id' ) ) ) {
			$labora_frontpageid = get_option( 'woocommerce_cart_page_id' );
		} else {
			$labora_frontpageid = $post->ID;
		}
	} else {
		$labora_frontpageid = $post->ID;
	}
}
$labora_pageslider = get_post_meta( $labora_frontpageid, 'labora_page_slider', true );

// General Preloader
$labora_page_preloader = get_option( 'labora_page_preloader' ) ? get_option( 'labora_page_preloader' ) : '';
if ( $labora_page_preloader != 'on' ) {
	echo '<div class="labora_page_loader"></div>';
}

	// Adds Page Background for Boxed Layout
if ( get_post_meta( $labora_frontpageid, 'labora_page_bg_image', true ) ) { ?>
	<img id="pagebg" style="background-image:url(<?php echo esc_url( get_post_meta( $labora_frontpageid, 'labora_page_bg_image', true ) ); ?>)" />
<?php } ?>

<div class="bodyoverlay"></div>

	<?php if ( get_option( 'labora_stickybar' ) !== 'on' &&  is_active_sidebar( 'labora_stickycontent' ) ) {  ?>
		<div id="trigger" class="tarrow"><i class="fa fa-arrow-circle-o-down fa-lg"></i></div>
		<div id="sticky">
		<?php
		if ( is_active_sidebar( 'labora_stickycontent' ) ) :
			dynamic_sidebar( 'labora_stickycontent' );
		endif; ?>
		</div>
	<?php } ?>

	<div id="wrapper">
		<?php
		
		$labora_headerstyle = get_option( 'labora_headerstyle' ) ? get_option( 'labora_headerstyle' ) : '';

		// Switch Headers
		switch ( $labora_headerstyle ) {
			case 'headerstyle1':
				get_template_part( 'headers/header','style1' );
				break;
			case 'headerstyle2':
				get_template_part( 'headers/header','style2' );
				break;
			case 'headerstyle3':
				get_template_part( 'headers/header','style3' );
				break;
			case 'headerstyle4':
				get_template_part( 'headers/header','style4' );
				break;
			case 'headerstyle5':
				get_template_part( 'headers/header','style5' );
				break;
			case 'fixedheader':
				get_template_part( 'headers/header','fixed' );
				break;
			case 'verticalleftmenu':
				get_template_part( 'headers/vertical','leftmenu' );
				break;
			default:
				get_template_part( 'headers/header','default' );
		}


		if ( is_front_page() || $labora_pageslider != '' ) {
			// Get Slider based on the option selected in theme options panel
			if ( get_option( 'labora_slidervisble' ) !== 'on' ) {

				if (! $labora_pageslider ) {
					$labora_chooseslider = get_option( 'labora_slider' );
				} else {
					$labora_chooseslider = $labora_pageslider;
				}
				switch ( $labora_chooseslider ) :
					case 'static_image':
						get_template_part( 'slider/static', 'slider' );
						break;
					case 'flexslider':
						get_template_part( 'slider/flex', 'slider' );
						break;
					case 'customslider':
						get_template_part( 'slider/custom', 'slider' );
						break;
					case 'default':
						get_template_part( 'slider/default', 'slider' );
						break;
					default:
						get_template_part( 'slider/default', 'slider' );
				endswitch;
			}
		} else {
			if ( ! is_404() ) {
				labora_generator( 'labora_subheader', $labora_frontpageid );
			}
		}
		// Conditional code for ending div for header style 4 and 5
		// starting div can be found in header-style4.php and header-style5.php
		if (
			( ! is_front_page() && $labora_pageslider != '' ) ||
			( ( ! is_front_page() || $labora_pageslider != '' ) && $labora_headerstyle == 'headerstyle4' ) ||
			( ( ! is_front_page() || $labora_pageslider != '' ) && $labora_headerstyle == 'headerstyle5' )
		) {
			// Adds div for the header style 4 and 5
			// starting div can be found in header-style4.php and header-style5.php
			echo '</div>';
		}
		$labora_sub_disabled = get_post_meta( $labora_frontpageid, 'labora_subheader_teaser_options', true );
		if ( ($labora_sub_disabled == 'disable') || ( get_option('labora_subheader') == 'disable' ) ) {
			echo wp_kses_post( labora_generator( 'labora_breadcrumb', $labora_frontpageid ) );
		}
		?>
<div id="main">
<?php
    

