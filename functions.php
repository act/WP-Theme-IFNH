<?php
function labora_child_scripts( $file ) {
	$child_file = str_replace( get_template_directory_uri(), get_stylesheet_directory_uri(), $file );
	return( $child_file );
}
/**
 * Enqueue the files in child theme
 * If you wish to change any js file or css file then use 'labora_child_scripts' function for that specific file and place it in relevant folder.
 * for eg:wp_register_script('iva-countTo', labora_child_scripts(THEME_JS . '/jquery.countTo.js'), 'jquery','1.0','in_footer'); 
 */
function labora_child_require_file( $file ) {
	$child_file = str_replace( get_template_directory(), get_stylesheet_directory(), $file );
	if ( file_exists( $child_file ) ) {
		return( $child_file );
	} else {
		return( $file );
	}
}

/**
 * Register and enqueue scripts.
 */
if ( ! function_exists( 'labora_theme_enqueue_scripts' ) ) {
	function labora_theme_enqueue_scripts() {

		$labora_theme_data	= wp_get_theme();
		$labora_theme_version = $labora_theme_data->get( 'Version' );

		global $post;

		// Enqueue scripts.
		wp_enqueue_script( 'hoverintent', LABORA_THEME_JS . '/hoverIntent.js',array( 'jquery' ),'', true );
		wp_enqueue_script( 'superfish', LABORA_THEME_JS . '/superfish.js',array( 'jquery' ),'',true );

		if ( is_singular( 'gallery' ) || ( is_a( $post, 'WP_Post' ) && has_shortcode( $post->post_content, 'gallery' ) )  || is_page_template( 'template-blog.php' ) ) {
			wp_enqueue_script( 'prettyphoto', LABORA_THEME_JS . '/jquery.prettyPhoto.js','','',true );
			wp_enqueue_style( 'prettyphoto', LABORA_THEME_CSS . '/prettyPhoto.css', array(), $labora_theme_version, false );
		}
		wp_enqueue_script( 'labora-customjs', LABORA_THEME_JS . '/labora-custom.js', array( 'jquery' ), $labora_theme_version , true );

		// AJAX URL
		$labora_data['ajaxurl'] = admin_url( 'admin-ajax.php' );

		// HOME URL
		$labora_data['home_url'] = get_home_url();

		// Directory URI
		$labora_data['directory_uri'] = get_template_directory_uri();

		// Pass data to javascript
		$labora_params = array(
			'l10n_print_after' => 'labora_localize_script_param = ' . wp_json_encode( $labora_data ) . ';',
		);
		wp_localize_script( 'jquery', 'labora_localize_script_param', $labora_params );

		/**
		 * enqueue styles.
		 */
		wp_enqueue_style( 'labora_parent_style', get_template_directory_uri() . '/style.css' );
		wp_enqueue_style( 'labora_child_style',
		    get_stylesheet_directory_uri() . '/style.css',
		    array('labora_parent_style')
		);
		wp_enqueue_style( 'font-awesome', LABORA_THEME_CSS . '/fontawesome/css/font-awesome.min.css' );
		wp_enqueue_style( 'labora-responsive', LABORA_THEME_CSS . '/responsive.css',$labora_theme_version, 'all' );
	}
	add_action( 'wp_enqueue_scripts','labora_theme_enqueue_scripts' );
}
/**
 * Flex Slider Enqueue Scripts
 */
if ( ! function_exists( 'labora_flexslider_enqueue_scripts' ) ) {
	add_action( 'labora_theme_flexslider','labora_flexslider_enqueue_scripts' );
	function labora_flexslider_enqueue_scripts() {
		$fs_slidespeed 	 = get_option( 'labora_flexslidespeed' ) ? get_option( 'labora_flexslidespeed' ) : '3000';
		$fs_slideeffect  = get_option( 'labora_flexslideeffect' ) ? get_option( 'labora_flexslideeffect' ) : 'fade';
		$fs_slidednav	 = get_option( 'labora_flexslidednav' ) ? get_option( 'labora_flexslidednav' ) : 'true';
		$flexslider_args = array(
								'slideeffect' => $fs_slideeffect,
								'slidespeed'  => $fs_slidespeed,
								'slidednav'	  => $fs_slidednav,
							);
		wp_enqueue_script( 'flexslider', LABORA_THEME_JS . '/jquery.flexslider-min.js', array( 'jquery' ), '', true );
		wp_localize_script( 'flexslider', 'flexslider_args', $flexslider_args );
		wp_enqueue_style( 'flexslider-style', LABORA_THEME_CSS . '/flexslider.css' );
	}
}



/**
 * Theme Class
 */
 if ( ! class_exists( 'Labora_Theme_Functions' ) ) {
 	class Labora_Theme_Functions {
		public $labora_meta_box;
		public function __construct() {
			$this->labora_theme_constants();
			$this->labora_theme_support();
			$this->labora_theme_admin_scripts();
			$this->labora_theme_admin_interface();
			$this->labora_theme_custom_widgets();
			$this->labora_theme_custom_meta();
			$this->labora_theme_meta_generator();
			$this->labora_theme_extras();
		}

		function labora_theme_constants() {

			/**
			 * Set the file path based on whether the Options
			 * Framework is in a parent theme or child theme
			 * Directory Structure
			 */

			$labora_theme_data	= wp_get_theme();
			$labora_theme_name = $labora_theme_data->get( 'Name' );

			define( 'LABORA_FRAMEWORK', '2.0' );
			define( 'LABORA_THEME_NAME', $labora_theme_name );
			define( 'LABORA_THEME_URI', get_template_directory_uri() );
			define( 'LABORA_THEME_DIR', get_template_directory() );
			define( 'LABORA_THEME_JS', LABORA_THEME_URI . '/js' );
			define( 'LABORA_THEME_CSS', LABORA_THEME_URI . '/css' );
			define( 'LABORA_FRAMEWORK_DIR', LABORA_THEME_DIR . '/framework/' );
			define( 'LABORA_FRAMEWORK_URI', LABORA_THEME_URI . '/framework/' );
			define( 'LABORA_ADMIN_URI',  LABORA_FRAMEWORK_URI . 'admin' );
			define( 'LABORA_ADMIN_DIR',  LABORA_FRAMEWORK_DIR . 'admin' );
			define( 'LABORA_THEME_WIDGETS',  LABORA_FRAMEWORK_DIR . 'widgets/' );
			define( 'LABORA_THEME_CUSTOM_META',  LABORA_FRAMEWORK_DIR . 'custom-meta/' );
		}
		/**
		 * allows a theme to register its support of a certain features
		 */
		function labora_theme_support() {

			add_theme_support( 'post-formats', array(
				'aside',
				'audio',
				'link',
				'image',
				'gallery',
				'quote',
				'status',
				'video',
				'event',
			));
			add_theme_support( 'post-thumbnails' );
			add_theme_support( 'automatic-feed-links' );
			add_editor_style( 'editor-style.css' );
			add_theme_support( 'title-tag' );
			add_theme_support( 'menus' );

			// Register menu.
			register_nav_menus( array(
				'primary-menu' => esc_html__( 'Primary Menu', 'labora' ),
			));


			// Define content width.
			if ( ! isset( $content_width ) ) {
				$content_width = 1100;
			}
		}
		/**
		 * scripts and styles enqueue .
		 */
		function labora_theme_admin_scripts() {
			$this->child_require_once( LABORA_FRAMEWORK_DIR . 'common/admin-scripts.php' );
		}
		/**
		 * Admin interface .
		 */
		function labora_theme_admin_interface() {
			$this->child_require_once( LABORA_FRAMEWORK_DIR . 'common/iva-googlefont.php' );
			$this->child_require_once( LABORA_FRAMEWORK_DIR . 'admin/admin-interface.php' );
			$this->child_require_once( LABORA_FRAMEWORK_DIR . 'admin/theme-options.php' );
		}
		/**
		 * Widgets .
		 */
		function labora_theme_custom_widgets() {
			$this->child_require_once( LABORA_THEME_WIDGETS . '/register-widget.php' );
			$this->child_require_once( LABORA_THEME_WIDGETS . '/iva-wg-contactinfo.php' );
			$this->child_require_once( LABORA_THEME_WIDGETS . '/iva-wg-sociable.php' );
			$this->child_require_once( LABORA_THEME_WIDGETS . '/iva-wg-recentpost.php' );
			$this->child_require_once( LABORA_THEME_WIDGETS . '/iva-wg-icon-box.php' );
			$this->child_require_once( LABORA_THEME_WIDGETS . '/iva-wg-button.php' );
		}
		/** load meta generator templates
		 * @files slider, Menus, page, posts,
		 */
		function LABORA_THEME_CUSTOM_META() {
			$this->child_require_once( LABORA_THEME_CUSTOM_META . '/page-meta.php' );
			$this->child_require_once( LABORA_THEME_CUSTOM_META . '/post-meta.php' );
			$this->child_require_once( LABORA_THEME_CUSTOM_META . '/slider-meta.php' );
			$this->child_require_once( LABORA_THEME_CUSTOM_META . '/testimonial-meta.php' );
			$this->child_require_once( LABORA_THEME_CUSTOM_META . '/gallery-meta.php' );
		}

		function labora_theme_meta_generator() {
			$this->child_require_once( LABORA_THEME_CUSTOM_META . '/meta-generator.php' );
		}

		/**
		 * Theme functions
		 * @uses skin generat$this->child_require_once(or
		 * @uses pagination
		 * @uses sociables
		 * @uses Aqua imageresize // Credits : http://aquagraphite.com/
		 * @uses plugin activation class
		 */
		function labora_theme_extras() {
			$this->child_require_once( LABORA_THEME_DIR . '/css/skin.php' );
			$this->child_require_once( LABORA_FRAMEWORK_DIR . 'includes/mega-menu.php' );
			$this->child_require_once( LABORA_FRAMEWORK_DIR . 'common/iva-generator.php' );
			$this->child_require_once( LABORA_FRAMEWORK_DIR . 'includes/image-resize.php' );
			$this->child_require_once( LABORA_FRAMEWORK_DIR . 'includes/class-activation.php' );
		}
		
		
		function child_require_once( $file ) {
			$child_file = str_replace(get_template_directory(), get_stylesheet_directory(), $file );
			if ( file_exists( $child_file ) ) {
				require_once( $child_file );
			} else {
				require_once( $file );
			}
		}
		
		/**
		 * Custom switch case for fetching
		 * posts, post-types, custom-taxonomies, tags
		 */
		 function labora_get_vars( $type ) {
			$labora_tax_options = array();
			switch ( $type ) {
				/**
				 * Get page titles.
				 */
				case 'pages':
					$labora_get_pages = get_pages( 'sort_column=post_parent,menu_order' );
					if ( ! empty( $labora_get_pages ) && ! is_wp_error( $labora_get_pages ) ) {
						foreach ( $labora_get_pages as $page ) {
							$labora_tax_options[ $page->ID ] = $page->post_title;
						}
					}
					break;
				/**
				 * Get slider slug and name.
				 */
				case 'slider':
					$labora_slider_cats = get_terms( 'slider_cat', 'orderby=name&hide_empty=0' );
					if ( ! empty( $labora_slider_cats ) && ! is_wp_error( $labora_slider_cats ) ) {
						foreach ( $labora_slider_cats as $slider ) {
							$labora_tax_options[ $slider->slug ] = $slider->name;
						}
					}
					break;

				/**
				 * Get posts slug and name.
				 */
				case 'posts':
					$labora_post_cats = get_categories( 'hide_empty=0' );
					if ( ! empty( $labora_post_cats ) && ! is_wp_error( $labora_post_cats ) ) {
						foreach ( $labora_post_cats as $cat ) {
							$labora_tax_options[ $cat->slug ] = $cat->name;
						}
					}
					break;

				/**
				 * Get categories slug and name.
				 */
				case 'categories':
					$labora_post_cats = get_categories( 'hide_empty=true' );
					if ( ! empty( $labora_post_cats ) && ! is_wp_error( $labora_post_cats ) ) {
						foreach ( $labora_post_cats as $cat ) {
							$labora_tax_options[ $cat->term_id ] = $cat->name;
						}
					}
					break;

				/**
				 * Get taxonomy tags.
				 */
				case 'tags':
					$labora_tags = get_tags(array(
						'taxonomy' => 'post_tag',
					) );
					if ( ! empty( $labora_tags ) && ! is_wp_error( $labora_tags ) ) {
						foreach ( $labora_tags as $tags ) {
							$labora_tax_options[ $tags->slug ] = $tags->name;
						}
					}
					break;

				/**
				 * Slider arrays for theme options.
				 */
				case 'slider_type':
					$labora_tax_options = array(
						'' 				=> esc_html__( 'Select Slider', 'labora' ),
						'flexslider' 	=> esc_html__( 'Flex Slider', 'labora' ),
						'static_image' 	=> esc_html__( 'Static Image', 'labora' ),
						'customslider'	=> esc_html__( 'Custom Slider', 'labora' ),
					);
					break;
			}

			return $labora_tax_options;
		}
	}
}
/**
 * Section to decide whether use child theme or parent theme 
 */
if ( ! defined('LABORA_NICHE_DIR') ) {
	define( 'LABORA_NICHE_DIR', labora_child_require_file( get_template_directory() . '/labora-niche/') );
}

/**Custom by Obergine **/
	 /*For svg upload*/
function cc_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}

add_filter('upload_mimes', 'cc_mime_types');

/*** ninja forms customisation ***/
function cpt_prepopulate_forms($options, $settings) {
	global $post;
    if( $settings['id'] == 27 ) // field ID for Research Interest
    {         
        $categories = get_terms( array(
                'taxonomy' => 'cases_category',
                'hide_empty' => false,
                   'meta_query' =>  array(
			            'key'	=> 'show_on_registration',
			            'value'	=> no
		            )
                ));

        foreach ( $categories as $category ) {
            $options[] = array(
                'label' =>  $category->name,
                'value' =>  $category->name,
                'calc'  =>  null,
                'selected' => $sel
            );
        }
    }
	
  
	return $options;
}
add_filter('ninja_forms_render_options','cpt_prepopulate_forms', 10, 2); 


//Creating 'Staff' post (category - 'Member') after ninja form submission 
add_action( 'ninja_forms_after_submission', 'create_page_from_ninjaform', 10, 1);

function create_page_from_ninjaform( $form_data ){    

      $form_fields = $form_data[ 'fields' ];

	  //field ids need to be changed for stage and live
      $title = $form_fields[ 6 ][ 'value' ];  //124 - dev, 202 - stage, live - 6
      $jobrole = $form_fields[ 7 ][ 'value' ];  //125 - dev, 203 - stage, live - 7
      $dept = $form_fields[ 8 ][ 'value' ];  //126 - dev, 204 - stage, live - 8
      $email = $form_fields[ 9 ][ 'value' ];  //127 - dev, 205 - stage, live - 9
      $phone = $form_fields[ 10 ][ 'value' ];  //128 - dev, 206 - stage, live - 10
	  $rivaluesPrim = $form_fields[ 18 ][ 'value' ];  //131 - dev, 209 - stage , , live - 18
	  $rivaluesSecondary = $form_fields[ 13 ][ 'value' ]; //132 - dev, 210 - stage, live -13 
      $content = $form_fields[ 14 ][ 'value' ]; //133 - dev, 211 - stage, live - 14
      $linkedin = $form_fields[ 11 ][ 'value' ]; //129 - dev, 207 - stage, live - 11
	  $p_link = $form_fields[ 12 ][ 'value' ]; //130 - dev, 208 - stage, live - 12
	  

	  $password = $form_fields[ 19 ][ 'value' ]; //138 - dev, 215 - stage, live - 19
	  

    $new_post = array(
        'post_title' => $title,
        'post_content' => $content,
        'post_status' => 'pending',
        'post_type' => 'staff',
        'category_name'   => 'Member', 
    );

    $new_post_id = wp_insert_post( $new_post );

    update_post_meta( $new_post_id, 'labora_department', $dept );    
    update_post_meta( $new_post_id, 'labora_phone', $phone );
    update_post_meta( $new_post_id, 'labora_email', $email );
    update_post_meta( $new_post_id, 'labora_sociable', $linkedin );

    update_post_meta( $new_post_id, 'profile_link', $p_link );    
	update_post_meta( $new_post_id, 'job_role', $jobrole );
	update_post_meta( $new_post_id, 'primary_research_interest', $rivaluesPrim );
	update_post_meta( $new_post_id, 'secondary_research_interest', $rivaluesSecondary );

	update_post_meta( $new_post_id, 'member_username', $email );
	update_post_meta( $new_post_id, 'member_password', $password );
   
	wp_set_post_terms( $new_post_id, array( 23 ), 'staff_category'  );   //setting category to 'Member' - id stage, dev (49), for live - 23
	
} 


//hiding menu items service, gallery and vacancy from admin menu
function hide_menu_items() {
    remove_menu_page( 'edit.php?post_type=service' );
    remove_menu_page( 'edit.php?post_type=gallery' );
    remove_menu_page( 'edit.php?post_type=vacancy' );
     remove_menu_page( 'edit.php?post_type=testimonialtype' );
}
add_action( 'admin_menu', 'hide_menu_items' );

function register_publications() {

	/**
	 * Post Type: PublicationItems.
	 */

	$labels = array(
		"name" => __( "PublicationItems", "labora-child" ),
		"singular_name" => __( "PublicationItem", "labora-child" ),
	);

	$args = array(
		"label" => __( "PublicationItems", "labora-child" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "publicationsslug", "with_front" => true ),
		"query_var" => true,
		"supports" => array( "title", "editor", "thumbnail" ),
	);

	register_post_type( "publicationsslug", $args );
}

add_action( 'init', 'register_publications' );

/*redirects for publications*/
add_action( 'template_redirect', 'subscription_redirect_post' );

function subscription_redirect_post() {
  $queried_post_type = get_query_var('post_type');
  if ( is_single() && 'publicationsslug' ==  $queried_post_type ) {
    wp_redirect( get_post_meta( get_the_ID(), 'wprss_item_permalink', true ), 301 );
    exit;
  }
}

//Getting only year in date on Publications page
function PubDateFormat($date) {			
    $datearr = explode("-", $date);
    return($datearr[0]);
}


/*** 21/02/2018 - b */
/** Custom post types and taxonomies */

function cptui_register_my_cpts_resourcepage() {

	/**
	 * Post Type: Resource Pages.
	 */

	$labels = array(
		"name" => __( "Resource Pages", "labora-child" ),
		"singular_name" => __( "Resource Page", "labora-child" ),
	);

	$args = array(
		"label" => __( "Resource Pages", "labora-child" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "/research/resources", "with_front" => true ),
		"query_var" => true,
		"supports" => array( "title", "editor", "thumbnail" ),
	);

	register_post_type( "resourcepage", $args );
}

add_action( 'init', 'cptui_register_my_cpts_resourcepage' );


function cptui_register_my_cpts_resourceitem() {

	/**
	 * Post Type: Resource Items.
	 */

	$labels = array(
		"name" => __( "Resource Items", "labora-child" ),
		"singular_name" => __( "Resource Item", "labora-child" ),
	);

	$args = array(
		"label" => __( "Resource Items", "labora-child" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "resourceitem", "with_front" => true ),
		"query_var" => true,
		"supports" => array( "title" ),
	);

	register_post_type( "resourceitem", $args );
}

add_action( 'init', 'cptui_register_my_cpts_resourceitem' );


function cptui_register_my_taxes_resourcecategory() {

	/**
	 * Taxonomy: Resource Categories.
	 */

	$labels = array(
		"name" => __( "Resource Categories", "labora-child" ),
		"singular_name" => __( "Resource Category", "labora-child" ),
	);

	$args = array(
		"label" => __( "Resource Categories", "labora-child" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => true,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'resourcecategory', 'with_front' => true, ),
		"show_admin_column" => false,
		"show_in_rest" => true,
		"rest_base" => "resourcecategory",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => false,
		);
	register_taxonomy( "resourcecategory", array( "resourcepage" ), $args );
}
add_action( 'init', 'cptui_register_my_taxes_resourcecategory' );




/*** 21/02/2018 - e */

//adding new roles - unapproved member and member  - begins
/*
add_role(
    'unapprovedmember',
    __( 'Unapproved Member' ),
    array(
        'read'         => false,  // false doesn't allows this capability
        'edit_posts'   => false,
    )
);

add_role(
    'member',
    __( 'Member' ),
    array(
        'read'         => true,  // true allows this capability
        'edit_posts'   => false,
    )
); */
//adding new roles - unapproved member and member  - ends

//Registering Query var name 'contentforid' globally as it was ignored by default
add_filter('query_vars', 'add_my_var');
function add_my_var($public_query_vars) {
    $public_query_vars[] = 'contentforid';
    return $public_query_vars;
}


// Displaying Custom fields on User edit form - begins
/*add_action( 'show_user_profile', 'my_custom_user_profile_field' );
add_action( 'edit_user_profile', 'my_custom_user_profile_field' );
function my_custom_user_profile_field( $user ) { ?>
    <h3>IFNH - Custom Fields</h3>
    <table class="form-table">
        <tr>
            <th><label for="Title">Title</label></th>
            <td>
                <input name="Title" id="Title" value="<?php echo esc_attr( get_the_author_meta( 'Title', $user->ID ) ); ?>" class="regular-text" type="text">
            </td>
        </tr>
    </table>

    <table class="form-table">
        <tr>
            <th><label for="JobRole">Job Role</label></th>
            <td>
                <input name="JobRole" id="JobRole" value="<?php echo esc_attr( get_the_author_meta( 'Job Role', $user->ID ) ); ?>" class="regular-text" type="text">
            </td>
        </tr>
    </table>

    <table class="form-table">
        <tr>
            <th><label for="Department">School / Department / Organisation</label></th>
            <td>
                <input name="Department" id="Department" value="<?php echo esc_attr( get_the_author_meta( 'Department', $user->ID ) ); ?>" class="regular-text" type="text">
            </td>
        </tr>
    </table>

    <table class="form-table">
        <tr>
            <th><label for="Phone">Phone</label></th>
            <td>
                <input name="Phone" id="Phone" value="<?php echo esc_attr( get_the_author_meta( 'Phone', $user->ID ) ); ?>" class="regular-text" type="text">
            </td>
        </tr>
    </table>
	
	 <table class="form-table">
        <tr>
            <th><label for="LinkedinProfileUrl">Linkedin Profile Url</label></th>
            <td>
                <input name="LinkedinProfileUrl" id="LinkedinProfileUrl" value="<?php echo esc_attr( get_the_author_meta( 'Linkedin Profile Url', $user->ID ) ); ?>" class="regular-text" type="text">
            </td>
        </tr>
    </table>
	
	 <table class="form-table">
        <tr>
            <th><label for="UniversityProfileUrl">University Profile Url</label></th>
            <td>
                <input name="UniversityProfileUrl" id="UniversityProfileUrl" value="<?php echo esc_attr( get_the_author_meta( 'University Profile Url', $user->ID ) ); ?>" class="regular-text" type="text">
            </td>
        </tr>
    </table>
	
	<table class="form-table">
        <tr>
            <th><label for="PrimaryInterest">Primary Interest</label></th>
            <td>
                <input name="PrimaryInterest" id="PrimaryInterest" value="<?php echo esc_attr( get_the_author_meta( 'Primary Interest', $user->ID ) ); ?>" class="regular-text" type="text">
            </td>
        </tr>
    </table>
	
	 <table class="form-table">
        <tr>
            <th><label for="SecondaryInterest">Secondary Interest</label></th>
            <td>
                <input name="SecondaryInterest" id="SecondaryInterest" value="<?php echo esc_attr( get_the_author_meta( 'Secondary Interest', $user->ID ) ); ?>" class="regular-text" type="text">
            </td>
        </tr>
    </table>
	
	 <table class="form-table">
        <tr>
            <th><label for="BioinComments">Bio in Comments</label></th>
            <td>
                <input name="BioinComments" id="BioinComments" value="<?php echo esc_attr( get_the_author_meta( 'Bio in Comments', $user->ID ) ); ?>" class="regular-text" type="text">
            </td>
        </tr>
    </table>
    
<?php }


add_action( 'personal_options_update', 'save_my_custom_user_profile_field' );
add_action( 'edit_user_profile_update', 'save_my_custom_user_profile_field' );
function save_my_custom_user_profile_field( $user_id ) {
    if ( !current_user_can( 'edit_user', $user_id ) )
        return false;
    update_user_meta( absint( $user_id ), 'Title', wp_kses_post( $_POST['Title'] ) );
    update_user_meta( absint( $user_id ), 'Job Role', wp_kses_post( $_POST['JobRole'] ) );
    update_user_meta( absint( $user_id ), 'Department', wp_kses_post( $_POST['Department'] ) );
    update_user_meta( absint( $user_id ), 'Phone', wp_kses_post( $_POST['Phone'] ) );	
	update_user_meta( absint( $user_id ), 'Linkedin Profile Url', wp_kses_post( $_POST['LinkedinProfileUrl'] ) );
	update_user_meta( absint( $user_id ), 'University Profile Url', wp_kses_post( $_POST['UniversityProfileUrl'] ) );
	update_user_meta( absint( $user_id ), 'Primary Interest', wp_kses_post( $_POST['PrimaryInterest'] ) );
	update_user_meta( absint( $user_id ), 'Secondary Interest', wp_kses_post( $_POST['SecondaryInterest'] ) );
	update_user_meta( absint( $user_id ), 'Bio in Comments', wp_kses_post( $_POST['BioinComments'] ) );
}

// Displaying Custom fields on User edit form - ends

//including shortcodes
include "members_grid.php";*/


//redirecting 'resourcepage' post types to login page when the user is not logged in 
add_action( 'template_redirect', 'redirect_to_specific_page' );
function redirect_to_specific_page() {    

	//redirecting to home page when member is logged in and trying to access Login template
	if ((get_page_template_slug() == 'members-login.php') && isset($_COOKIE['loggedin_user']) && $_COOKIE['loggedin_user'] == '1')
	{
		wp_redirect('/ifnh/?loggedin=yes');
		exit;
	}

	//

    if (is_singular('resourcepage'))
    {
        if(get_field('is_it_secure', get_the_ID()) != 1)
        {
            return;
        }        
        else
        { 
			
			if (isset($_COOKIE['loggedin_user']) && $_COOKIE['loggedin_user'] == '1')
			{
				return;
			}
			else
			{
				wp_redirect('/ifnh/member-login?pagesecure=yes');
				exit;
			}
        }
    }    

}


//encrypting password  for members when creating the post type

function my_function_encrypt_passwords( $value, $post_id, $field  )  
{  
    $value = wp_hash_password( $value );     
    return $value;  
}  
add_filter('acf/update_value/name=member_password', 'my_function_encrypt_passwords', 10, 3);