<?php

		function ifnh_vc_staff_shortcode( $atts ) {
			extract(shortcode_atts( array(
				'items' 	=> '3',
				'display_type' => 'grid',
				'orderby' 	=> 'title',
				'order' 	=> 'ASC',
			), $atts));

			global $post, $wp_query;

			if ( get_query_var( 'paged' ) ) {
				$paged = get_query_var( 'paged' );
			} elseif ( get_query_var( 'page' ) ) {
				$paged = get_query_var( 'page' );
			} else {
				$paged = 1;
			}

			$width = '350';	$height = '250';

			$staff_args = array(
				'post_type'	   	=> 'staff',
				'posts_per_page' => $limit,
				'paged'		=> $paged,
				'tax_query' => array(
					'relation' => 'OR',
				),
				'orderby'	=> $orderby,
				'order'		=> $order,
			);
			if ( '' != $cat ) {
				$cats = explode( ',',$cat );
					$tax_cat = array(
						'taxonomy' 	=> 'staff_category',
						'field' 	=> 'slug',
						'terms' 	=> $cats,
					);
					array_push( $staff_args['tax_query'],$tax_cat );
			}
			$original_query = $wp_query;
			$staff_post_type = new WP_Query( $staff_args );
			$wp_query = $staff_post_type;


			$out = $c_pagination = $class_to_filter = $css = '';

			$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ) );

			$css_class .= ' col_' . $columns;

			if ( $pagination == 'yes' ) {
				$c_pagination = 'true';
			} else {
				$c_pagination = 'false';
			}

			if ( $display_type === 'list' ) {
				if ( $staff_post_type->have_posts() ) :
					while ( $staff_post_type->have_posts() ) : $staff_post_type->the_post();

						$labora_department = get_post_meta( get_the_ID(), 'labora_department', true );
						$labora_address    = get_post_meta( get_the_ID(), 'labora_address', true );
						$labora_phone      = get_post_meta( get_the_ID(), 'labora_phone', true );
						$labora_email      = get_post_meta( get_the_ID(), 'labora_email', true );
						$labora_sociable   = get_post_meta( get_the_ID(), 'labora_sociable', true );

                        //
                        $staff_profile_link   = trim(get_post_meta( get_the_ID(), 'profile_link', true ));
                        //

						$out .= '<div class="at-person list">';
						if ( has_post_thumbnail( get_the_id() ) ) {
							$out .= '<div class="at-person-image">';

                            if ( $staff_profile_link != '' ) {
                                $out .= '<a target="_blank" href="' . $staff_profile_link . '">';
                            }

							$out .= labora_vc_resize( get_the_id(),'', $width, $height, '', get_the_title( get_the_ID() ) );

							 if ( $staff_profile_link != '' ) {
                                $out .= '</a>';
                            }

							$out .= '</div>';
						}
						$out .= '<div class="at-person-content"><h4>';

                        if ( $staff_profile_link != '' ) {
                                $out .= '<a target="_blank" href="' . $staff_profile_link . '">' . get_the_title() . '</a>';
                        }

                        else
                        {
                            $out .= get_the_title() ;
                        }

                        $out .= '</h4>';

						$out .= '<h6>' . $labora_department . '</h6>';
						$out .= '<p>' . get_the_excerpt() . '</p>';

						if ( $staff_profile_link != '' ) {
                                $out .= '<a target="_blank" class="read_more" href="' . $staff_profile_link . '"><span>' . esc_html__( 'View Profile', 'labora-vc-textdomain' ) . '</span></a>';
                        }


                        if ( function_exists( 'labora_social_icon' ) ) {
				            $out .= '<div class="social">';
				            $out .= labora_social_icon( $labora_sociable );
				            $out .= '</div>'; // Sociables end
			            }
						$out .= '</div>';
						$out .= '</div>'; //.staff-item
					endwhile;

					// staff pagination
					if ( function_exists( 'labora_pagination' ) ) {
						if ( $pagination == 'yes' ) {
							$out .= '<div class="clear"></div>';
							ob_start();
							$out .= labora_pagination();
							$out .= ob_get_contents();
							ob_end_clean();
						}
					}
					endif;
					wp_reset_query();
					$wp_query = $original_query;
			}
			if ( $display_type === 'grid' ) {
				if ( $staff_post_type->have_posts() ) :
					$out .= '<div class="at-person grid ' . esc_attr( $css_class ) . '"><ul>';
					while ( $staff_post_type->have_posts() ) : $staff_post_type->the_post();

						$labora_department = get_post_meta( get_the_ID(), 'labora_department', true );
						$labora_address    = get_post_meta( get_the_ID(), 'labora_address', true );
						$labora_phone      = get_post_meta( get_the_ID(), 'labora_phone', true );
						$labora_email      = get_post_meta( get_the_ID(), 'labora_email', true );
						$labora_sociable   = get_post_meta( get_the_ID(), 'labora_sociable', true );

                        //
                        $staff_profile_link   = trim(get_post_meta( get_the_ID(), 'profile_link', true ));
                        //

						$out .= '<li>';
						if ( has_post_thumbnail( get_the_id() ) ) {
							$out .= '<div class="at-person-image">';

                            if ( $staff_profile_link != '' ) {
                                $out .= '<a target="_blank" href="' . $staff_profile_link . '">';
                            }

							$out .= labora_vc_resize( get_the_id(),'', $width, $height, '', get_the_title( get_the_ID() ) );

                             if ( $staff_profile_link != '' ) {
                                $out .= '</a>';
                            }

							$out .= '</div>';
						}
						$out .= '<div class="at-person-content"><h4>';

                        if ( $staff_profile_link != '' ) {
                                $out .= '<a target="_blank" href="' . $staff_profile_link . '">' . get_the_title() . '</a>';
                        }

                        else
                        {
                            $out .= get_the_title() ;
                        }

                         $out .= '</h4>';

						$out .= '<h6>' . $labora_department . '</h6>';
						$out .= '<p>' . labora_substr_text( get_the_excerpt(), 95 ) . '</p>';


                        if ( $staff_profile_link != '' ) {
                                $out .= '<a target="_blank" class="read_more" href="' . $staff_profile_link . '"><span>' . esc_html__( 'View Profile', 'labora-vc-textdomain' ) . '</span></a>';
                        }


                        if ( function_exists( 'labora_social_icon' ) ) {
				            $out .= '<div class="social">';
				            $out .= labora_social_icon( $labora_sociable );
				            $out .= '</div>'; // Sociables end
			            }
						$out .= '</div>';
						$out .= '</li>'; //.staff-item
					endwhile;
					$out .= '</ul></div>';//.staffs_lists
					// staff pagination
					if ( function_exists( 'labora_pagination' ) ) {
						if ( $pagination == 'yes' ) {
							$out .= '<div class="clear"></div>';
							ob_start();
							$out .= labora_pagination();
							$out .= ob_get_contents();
							ob_end_clean();
						}
					}
				endif;
				wp_reset_query();
				$wp_query = $original_query;
			}
			return $out;
		}
	
add_shortcode( 'ifnh_vc_staff', array( $this, 'ifnh_vc_staff_shortcode' ) );
?>
