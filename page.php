<?php
	//Includes the header.php template file from your current theme's directory
	get_header();
?>
<div id="primary" class="pagemid">
	<div class="inner">
		<main class="content-area">
			<div class="entry-content-wrapper clearfix">

			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<?php the_content(); ?>
					<?php wp_link_pages( array( 'before' => '<div class="page-link">' . esc_html__( 'Pages:', 'labora' ), 'after' => '</div>' ) ); ?>
				</div><!-- #post-<?php the_ID(); ?> -->

				<?php endwhile; ?>
				<?php endif; ?>
				<?php comments_template( '', true ); ?>
			</div><!-- .entry-content-wrapper -->
		</main><!-- .content-area -->

		<?php if ( labora_generator( 'labora_sidebar_option', $post->ID ) !== 'fullwidth' ) { get_sidebar(); } ?>

		<div class="clear"></div>
	</div><!-- .inner -->
</div><!-- .pagemid -->
<?php
get_footer();
