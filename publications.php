
<?php /* Template Name: IFNH Publications */ ?>
<?php
	//Includes the header.php template file from your current theme's directory
	get_header();
?>

<div id="primary" class="pagemid">
	<div class="inner">
        <div class="entry-content-wrapper clearfix">
        <div class="vc_row wpb_row vc_row-fluid pubintro">
            <div class="">
                <div class="wpb_column vc_column_container vc_col-sm-8 vc_col-lg-offset-2 vc_col-lg-8 vc_col-md-offset-2 vc_col-md-8 vc_col-sm-offset-2 vc_col-xs-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
	                        <div class="wpb_text_column wpb_content_element  pageintro">
		                        <div class="wpb_wrapper">
			                        <p><?php echo get_field( "publications_intro" ); ?> </p>

		                        </div>
	                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

            <div class="rightsidebar pubrs">
                <main class="content-area">			

			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

				    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                       <?php the_content(); ?>
                   
					    <?php wp_link_pages( array( 'before' => '<div class="page-link">' . esc_html__( 'Pages:', 'labora' ), 'after' => '</div>' ) ); ?>
				    </div><!-- #post-<?php the_ID(); ?> -->

				    <?php endwhile; ?>
				    <?php endif; ?>
				    <?php comments_template( '', true ); ?>
			    <!-- .entry-content-wrapper -->
		    </main><!-- .content-area -->

		    <?php get_sidebar();  ?>

            </div>
		
        </div>
	</div><!-- .inner -->
</div><!-- .pagemid -->
<?php
get_footer();
