<?php get_header(); ?>


<!-- Variables -->
<?php
	$resource_cats = get_the_terms(get_the_ID(), 'resourcecategory');		

?>	

<div id="primary" class="pagemid">
	<div class="inner">

		<main class="content-area">

			<div class="entry-content-wrapper clearfix">
			<?php if ( have_posts() ) {
				
				while ( have_posts() ) : the_post(); ?>
				<div class="ContentFromCMS">
					<?php the_content(); ?>
				</div>
				<?php endwhile; ?>

				<div class="SidecarCategories">
				<?php
					foreach ( $resource_cats as $cat ) {
						if ($cat-> parent == 0) {
							echo '<a href="#'.str_replace(" ", "-", $cat->name).'">'.$cat->name.'</a>';
						}
					}
				} //end if
				?>
			</div>

			</div><!-- .entry-content-wrapper-->

			<?php
			
					foreach ( $resource_cats as $cat ) { 	

						if ($cat-> parent == 0) {
							$i = 0;	
							$ContentToLoadForID =0;
						$sub_resource_cats = get_terms( 'resourcecategory', array( 'child_of' => $cat->term_id ) );
						?>

						<div class="CatgeoryItem" id="<?php echo str_replace(" ", "-", $cat->name)?>">
							<h2><?php echo $cat->name; ?></h2>
							
							<div class="SubResourceCats">							
							<?php 
								$j = 0;
								foreach ( $sub_resource_cats as $subcat ) { 

									$suburl = get_permalink()."?contentforid=".$subcat -> term_id;
									$qs_contentforid = get_query_var('contentforid', '');

									if (!empty($qs_contentforid)) {										

										$ContentToLoadterm = get_term($qs_contentforid, 'resourcecategory');
										$termParentContentToLoadterm = ($ContentToLoadterm->parent == 0) ? $ContentToLoadterm : get_term($ContentToLoadterm->parent, 'resourcecategory');

									}


									if($j==0)
									{
										$ContentToLoadForID = $subcat->term_id;									

										echo '<a id="contentforid-'.$subcat->term_id.'" class="active" href="'.$suburl.'" title="'.$subcat -> name.'">'.$subcat -> name.'</a>';

									}
									else
									{										
										echo '<a id="contentforid-'.$subcat->term_id.'" href="'.$suburl.'" title="'.$subcat -> name.'">'.$subcat -> name.'</a>';
									}
									
									if (!empty($qs_contentforid)) {		

										if( ($termParentContentToLoadterm -> term_id) == ($cat -> term_id))
										{
											$ContentToLoadForID = $qs_contentforid;
										}
									}
									

									$j++;

								}
								
							?>
							</div>

							<?php
							if($i==0)
									{
										//

								echo '<div class="TabContent">';
								
								$the_resourcequery = new WP_Query(array(
									'post_type' => 'resourceitem',
									'post_status' => 'publish',
									'posts_per_page' => -1,
									'meta_key' => 'resource_category',
									'meta_value'  =>  $ContentToLoadForID
						
								)); 

					
								if( $the_resourcequery->have_posts() ) { ?>
								<div class="ResourceItem">
								<?php while( $the_resourcequery->have_posts() ) : $the_resourcequery->the_post(); 
									$rescatForItem=	get_field('resource_category');
									$restype=	get_field('resource_type');
									$resurl=	get_field('resource_url');
									$resgroupname=	get_field('group_name');

									if(($restype== "JPG") ||($restype== "PDF"))
									{
										$resfile = get_field('resource_media_file');
										if ($resfile) {	
											$resurl= $resfile;
										}
									}

									$resItemInnerClass= preg_replace('/[^A-Za-z\-]/', '', str_replace(" ", "-", $resgroupname)).'-'.str_replace(" ", "-", $cat -> name);

								?>

								<div class="ResourceItemInner-<?php echo $resItemInnerClass ?>">
								<?php 
											echo '<h5>'.$resgroupname.'</h5>';

											echo '<a target="_blank" title="'. get_the_title()  .'" class="'. $restype .'" href="'. $resurl .'">';													
											
										
											//echo 'ContentToLoadForID for: '. $rescatForItem. " : ".  $ContentToLoadForID . "<br/>";
											the_title(); 
											?>
										</a>
									</div>
								<?php endwhile; ?>
								</div> 
								<?php }
								else
								{
									echo "<p style='margin-bottom:0px;'>Sorry, no files exist here!</p>";
								}
							 wp_reset_query();
							 echo '</div>';
//

									}
									$i++;
									?>

						</div>

						
			<?php } 
	

			} 
			
			?>

			

		</main><!-- .content-area -->
		
		<div class="clear"></div>
	</div><!-- .inner -->
</div><!-- .pagemid -->



<?php
get_footer();
