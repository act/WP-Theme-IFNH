<?php get_header(); ?>
<div id="primary" class="pagemid">
	<div class="inner">

		<main class="content-area">

			<div class="entry-content-wrapper clearfix">

			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<?php get_template_part( 'post-formats/content', get_post_format() );?>
			<?php get_template_part( 'share','link' ); ?>

			<?php

			if ( get_option( 'labora_aboutauthor' ) !== 'on' ) {
				labora_generator( 'labora_about_author' );
			}

			if ( get_option( 'labora_relatedposts' ) !== 'on' ) {
				labora_generator( 'labora_related_posts', $post->ID );
			}

			if ( comments_open() || get_comments_number() ) {
				comments_template();
			}

			if ( get_option( 'labora_singlenavigation' ) !== 'on' ) {
				if ( is_singular( 'attachment' ) ) {
					// Parent post navigation.
					the_post_navigation( array(
						'prev_text' => _x( '<span class="meta-nav">Published in</span><span class="post-title">%title</span>', 'Parent post link', 'labora' ),
					) );
				} elseif ( is_singular( 'post' ) ) {
					// Previous/next post navigation.
					the_post_navigation( array(
						'next_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Next', 'labora' ) . '</span> ' .
							'<span class="screen-reader-text">' . __( 'Next post:', 'labora' ) . '</span> ' .
							'<span class="post-title">%title</span>',
						'prev_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Previous', 'labora' ) . '</span> ' .
							'<span class="screen-reader-text">' . __( 'Previous post:', 'labora' ) . '</span> ' .
							'<span class="post-title">%title</span>',
					) );
				}
			} else {
				posts_nav_link();
			}
			?>
			<?php endwhile; ?>
			<?php else : ?>
			<?php '<p>' . esc_html__( 'Sorry, no posts matched your criteria.', 'labora' ) . '</p>';?>
			<?php endif; ?>
			</div><!-- .entry-content-wrapper-->
		</main><!-- .content-area -->
		<?php if ( labora_generator( 'labora_sidebar_option', $post->ID ) !== 'fullwidth' ) { get_sidebar(); } ?>
		<div class="clear"></div>
	</div><!-- .inner -->
</div><!-- .pagemid -->
<?php
get_footer();
