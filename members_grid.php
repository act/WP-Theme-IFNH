<?php

function members_grid_fn() {
    ob_start();

?>

    <?php  

// Create the WP_User_Query object
$wp_user_query = new WP_User_Query( array( 'role' => 'Member' ) );
// Get the results
$members = $wp_user_query->get_results();

// Check for results
if ( ! empty( $members ) ) {
    echo '<div class="at-person grid  col_2 Member"><div class="MemOuter">';
    // loop through each member
    foreach ( $members as $member ) {
        // get all the user's data
        $member_info = get_userdata( $member->ID );
        //$member_info_meta = get_user_meta ( $member->ID );

        $primary_interest   = str_replace(" ","-",trim(get_user_meta( $member->ID, 'Primary Interest', true )));

        $staff_profile_link   = trim(get_user_meta( $member->ID, 'University Profile Url', true ));
        $user_jobrole = trim(get_user_meta( $member->ID, 'Job Role', true ));

        $MemberName= $member->first_name;
       
        echo   '<div class="'.$primary_interest.'">';

        echo '<div class="at-person-content">';

        echo '<h4>';
        if ( $staff_profile_link != '' ) {
            echo '<a target="_blank" href="' . $staff_profile_link . '">' . $MemberName . '</a>';
        }
        else
        {
            echo $MemberName;
        }

        echo '</h4>';
        echo '<h6>' . $user_jobrole . '</h6>';

        echo '</div>';
        echo   '</div>';

        // echo $author_info->first_name . ' ' . $author_info->last_name;
    }
    echo '</div></div>';
} 
else {
    echo 'No members found';
}

/////////////


    

    return ob_get_clean();
}

add_shortcode('members_grid', 'members_grid_fn');



