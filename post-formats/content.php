<?php
/**
 * The default template for displaying content
 * Used for both single and index/archive/search.
 * @package WordPress
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header">
		<?php
		if ( is_single() ) {
			the_title( '<h2 class="entry-title">', '</h2>' );
		} else {
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		}
		?>
		<?php if ( get_option( 'labora_postmeta' ) !== 'on' ) { ?>
			<div class="entry-meta">
				<?php labora_post_metadata(); ?>
			</div><!-- .entry-meta -->
		<?php } ?>

	</header><!-- .entry-header -->

	<?php
	if ( has_post_thumbnail() ) {
		echo '<div class="postimg-flat"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark"><figure>' . wp_kses_post( labora_img_resize( $post->ID, '', '1100', '550', '', '' ) ) . '</figure></a></div>';
	} ?>

	<div class="entry-content">
	<?php
	if ( has_excerpt() && ! is_single() ) {
		the_excerpt();
		echo '<a class="more-link" href="' . esc_url( get_permalink() ) . '">' . esc_html__( 'Continue Reading','labora' ) . '</a>';
	} else {
		the_content( esc_html__( 'Continue Reading', 'labora' ) );
		wp_link_pages( array(
			'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'labora' ) . '</span>',
			'after'       => '</div>',
			'link_before' => '<span>',
			'link_after'  => '</span>',
		));
	} ?>
	<?php if ( is_single() ) { the_tags( '<div class="tagcloud clearfix">', ' ', '</div>' );  } ?>
	</div><!-- .entry-content -->
</article><!-- /post-<?php the_ID();?> -->
<?php
