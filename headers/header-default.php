<?php
    /* Header Style Default */
?>
<?php
    if ( get_option( 'labora_topbar' ) != 'on' ) {
        if ( is_active_sidebar( 'topbarleft' ) || is_active_sidebar( 'topbarright' ) ) {
?>
<div class="topbar">
    <div class="inner">
        <div class="topbar-left">
            <?php  if ( is_active_sidebar( 'topbarleft' ) ) : dynamic_sidebar( 'topbarleft' );  endif; ?>
        </div><!-- /one_half -->
        <div class="topbar-right">
			<div id="ivaSearch" class="ivaSearch icnalign fa fa-search fa-fw"></div>
			
			<?php
			
                //if(! is_user_logged_in() )
                if (isset($_COOKIE['loggedin_user']) && $_COOKIE['loggedin_user'] == '1')
	            {   
				?>	
                	<!--<a class="TopHeaderBtn" title="My profile" href="/edit-profile">My profile</a> -->
				<a class="TopHeaderBtn" title="Logout" href="/ifnh/member-logout">Logout</a>		
				
			
				
				<?php
				
				
				} else{

				?>
                	<a class="TopHeaderBtn" title="Login" href="/ifnh/member-login">Login</a>
				    <a class="TopHeaderBtn" title="Registration" href="/ifnh/members/registration/">Register</a>
			
				<?php
					}
				?>
			
            <?php  //if ( is_active_sidebar( 'topbarright' ) ) : dynamic_sidebar( 'topbarright' );  endif; ?>
            
        </div><!-- /one_half last -->
    </div>
</div><!-- /topbar -->
<?php
        }
    }
?>
<header class="header-style-default">
    <div class="header">
        <div class="header-area">
            <div class="logo">
                <?php labora_generator( 'labora_logo', 'labora_header_dark_logo' ); ?>
            </div><!-- /logo -->
            <div class="primarymenu menuwrap">
                <?php labora_generator( 'labora_primary_menu' ); ?>
                <?php
                    
                    if ( has_nav_menu( 'primary-menu' ) ) {
                ?>
                <div id="iva-mobile-nav-icon" class="iva-mobile-dropdown"><span></span><span></span><span></span><span></span></div>
                <?php } ?>
            </div>


            <!-- Header Widget Area -->
            <?php  if ( is_active_sidebar( 'default_header_widget' ) ) : dynamic_sidebar( 'default_header_widget' );  endif; ?>
        </div>

        <div id="ivaSearchbar" class="act">
            <div class="inner">
                <form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                    <input type="text" value="" name="s" id="s" placeholder="<?php echo esc_html__( 'Search here...', 'labora' ); ?>" class="ivaInput headerSearch" />
                    <span class="search-close"><i class="fa fa-close fa-1"></i></span>
                </form>
            </div>
        </div>
        <?php
            // Mobile menu
            labora_generator( 'labora_mobile_menu' );
        ?>
        
    </div>
</header><!-- #header -->
<?php
