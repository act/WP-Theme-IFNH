<?php
if ( ! class_exists( 'Labora_Generator_Class' ) ) {
	class Labora_Generator_Class {

		// P R I M A R Y   M E N U
		//--------------------------------------------------------
		function labora_primary_menu() {
			if ( has_nav_menu( 'primary-menu' ) ) {
				wp_nav_menu(array(
					'container'     => false,
					'theme_location' => 'primary-menu',
					'menu_class'    => 'sf-menu',
					'menu_id'       => 'iva_menu',
					'echo'          => true,
					'before'        => '',
					'after'         => '',
					'link_before'   => '',
					'link_after'    => '',
					'depth'         => 0,
					'walker'        => new Labora_Custom_Menu_Walker(),
				));
			} else {
				echo '<ul class="iva_menu sf-menu"><li><a class="alignright" href="' . esc_url( home_url( '/' ) ) . 'wp-admin/nav-menus.php">' . esc_html__( 'WordPress Menu is not assigned or created.', 'labora' ) . '</a></li></ul>';
			}
		}
		function labora_mobile_menu() {
			if ( has_nav_menu( 'primary-menu' ) ) {
				wp_nav_menu(array(
					'container'     => 'div',
					'container_class' => 'iva-mobile-menu',
					'theme_location' => 'primary-menu',
					'menu_class'	=> 'iva_mmenu',
					'echo'          => true,
					'before'        => '',
					'after'         => '',
					'link_before'   => '',
					'link_after'    => '',
					'depth'         => 0,
					'walker'        => new Labora_Mobile_Custom_Menu_Walker(),
				));
			}
		}

		function labora_vertical_menu() {
			if (has_nav_menu( 'primary-menu' ) ) {
				wp_nav_menu(array(
					'container'     => 'div',
					'container_class'=> 'iva_vertical_menu',
					'theme_location'=> 'primary-menu',
					'menu_class'    => 'sf-menu sf-vertical',
					'menu_id'       => 'iva_menu',
					'echo'          => true,
					'before'        => '',
					'after'         => '',
					'link_before'   => '',
					'link_after'    => '',
					'depth'         => 0,
					'walker'        => new Labora_Custom_Menu_Walker()
				));
			}
		}

		// L O G O   G E N E R A T O R
		//--------------------------------------------------------
		function labora_logo( $labora_logoid ) {
			$labora_logo = get_option( 'labora_logo' );
			if ( $labora_logo == 'logo' ) {

				$labora_img_attachment_id = labora_get_attachment_id_from_src( get_option( $labora_logoid ) );
				$labora_image_attributes  = wp_get_attachment_image_src( $labora_img_attachment_id , 'full' ); // returns an array
				?>
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php bloginfo( 'name' ); ?>">
					<img src="<?php echo esc_url( $labora_image_attributes[0] ); ?>" alt="<?php bloginfo( 'name' ); ?>"  width="<?php echo esc_attr( $labora_image_attributes[1] ); ?>" height="<?php echo esc_attr( $labora_image_attributes[2] ); ?>" />
				</a>
				<?php
			} else { ?>
				<h1 id="site-title"><span><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?>	</a></span></h1>
				<h2 id="site-description"><?php echo bloginfo( 'description' ); ?></h2>
				<?php
			}
		}
		// S I D E B A R   P O S I T I O N S
		//--------------------------------------------------------
		function labora_sidebar_option( $postid ) {
			// Get sidebar class and adds sub class to pagemid block layout

			$labora_sidebar_layout = get_option( 'labora_defaultlayout' ) ? get_option( 'labora_defaultlayout' ) : 'rightsidebar';
			$labora_sidebaroption  = get_post_meta( $postid, 'labora_sidebar_options', true ) ? get_post_meta( $postid, 'labora_sidebar_options', true ): $labora_sidebar_layout;

			switch ( $labora_sidebaroption ) {
				case  'rightsidebar':
						$labora_sidebaroption = 'rightsidebar';
						break;
				case  'leftsidebar':
						$labora_sidebaroption = 'leftsidebar';
						break;
				case  'fullwidth':
						$labora_sidebaroption = 'fullwidth';
						break;

				default:
						$labora_sidebaroption = $labora_sidebaroption;
			}

			if ( class_exists( 'woocommerce' ) ) {
				if ( is_product() ) {
					$labora_sidebaroption = $labora_sidebaroption;
				}
			} else {
				if ( is_archive() || is_search() || is_tax( 'gallery_category' ) ) {
					$labora_sidebaroption = $labora_sidebar_layout;
				}
			}

			if ( get_query_var( 'eventDisplay' ) ) {
				if ( tribe_get_option( 'tribeEventsTemplate', 'default' ) == 'template_events_leftsidebar.php' ) {
					$labora_sidebaroption = $labora_sidebar_layout;
				}
			}
			if ( get_query_var( 'eventDisplay' ) ) {
				if ( tribe_get_option( 'tribeEventsTemplate', 'default' ) == 'template_events_rightsidebar.php' ) {
					$labora_sidebaroption = $labora_sidebar_layout;
				}
			}
			if ( is_404() ) {
				$labora_sidebaroption = 'fullwidth';
			}

			return $labora_sidebaroption;
		}

		/***
		 * P O S T   L I N K   T Y P E
		 *--------------------------------------------------------
		 * labora_post_link_to - generates URL based on link type
		 * @param - string link_type - Type of link
		 * @return - string URL
		 *
		 */
		function labora_post_link_to( $labora_link_type ) {

			global $post;

			//use switch to generate URL based on link type
			switch ( $labora_link_type ) {
				case 'linkpage':
						return get_page_link( get_post_meta( $post->ID, 'labora_linkpage', true ) );
						break;
				case 'linktocategory':
						return get_category_link( get_post_meta( $post->ID, 'labora_linktocategory', true ) );
						break;
				case 'linktopost':
						return get_permalink( get_post_meta( $post->ID, 'labora_linktopost', true ) );
						break;
				case 'linkmanually':
						return esc_url( get_post_meta( $post->ID, 'labora_linkmanually', true ) );
						break;
				case 'nolink':
						return 'nolink';
						break;
			}
		}

		/**
		 * P O S T   A T T A C H M E N T S
		 *--------------------------------------------------------
		 * labora_get_post_attachments - displays post attachements
		 * @param - int post_id - Post ID
		 * @param - string size - thumbnail, medium, large or full
		 * @param - string attributes - thumbnail, medium, large or full
		 * @param - int width - width to which image has be revised to
		 * @param - int height - height to which image has be revised to
		 * @return - string Post Attachments
		 */

		function labora_get_post_attachments( $postid = 0, $size = 'thumbnail', $attributes = '', $width, $height, $postlinkurl ) {

			global $post;

			//get the postid
			if ( $postid < 1 ) { $postid = get_the_ID(); }

			//variables
			$rel = $out = '';

			// get the attachments (images)
			$labora_images = get_children( array(
				'post_parent'    => $postid,
				'post_type'      => 'attachment',
				'order'          => 'DESC',
				'numberposts'    => 0,
				'post_mime_type' => 'image',
			) );

			//if images exists	, define/determine the relation for lightbox
			if ( count( $labora_images ) > 1 ) {
				$rel = '"group' . $postid . '"';
			} else {
				$rel = '""';
			}
			$rel = ' rel=' . $rel;

			//if images exists, loop through and prepare the output
			if ( $labora_images ) {
				$out .= '<div class="flexslider">';
				$out .= '<ul class="slides">';
				//loop through
				foreach ( $labora_images as $image ) {

					$labora_full_attachment = wp_get_attachment_image_src( $image->ID, 'full' );

					if ( ! empty( $image -> ID ) ) {
						$alt = get_the_title( $image->ID );
					}
					$out .= '<li>';
					$out .= labora_img_resize( '',$labora_full_attachment['0'], $width, $height,'',$alt );
					$out .= '</li>';
				}//loop ends
				$out .= '</ul>';
				$out .= '</div><div class="clear"></div>';
			} else { //if images does not exists
				$alt = '';
				$labora_post_thumbnail_id  = get_post_thumbnail_id( $postid );
				$labora_full_attachment 	= wp_get_attachment_image_src( $labora_post_thumbnail_id, 'full' );

				if ( ! empty( $labora_post_thumbnail_id ) ) {
					$alt = get_the_title( $labora_post_thumbnail_id );
				}
				$out .= '<figure><a href="' . esc_url( $postlinkurl ) . '">';
				$out .= labora_img_resize( '',$labora_full_attachment['0'],$width,$height,'imageborder',$alt );
				$out .= '</a></figure>';
			}// if images exists

			return $out;
		}

		// S U B H E A D E R
		//--------------------------------------------------------
		function labora_subheader( $postid ) {

			global $wp_query;

			$labora_sub_desc = $labora_sub_title   = $labora_subheader_props = $labora_sh_css = $out = '';

			$labora_sub_option         = get_post_meta( $postid, 'labora_subheader_teaser_options', true );
			$labora_page               = get_post( $postid );
			$labora_pagetitle_styling  = get_post_meta( $postid, 'labora_sub_styling', true );
			$labora_sh_bg_properties 	= get_post_meta( $postid,'labora_subheader_img', true );
			$labora_sh_breadcrumb      = get_post_meta( $postid, 'labora_breadcrumb', true );
			$labora_sh_txt_clr 		= get_post_meta( $postid,'labora_sh_txtcolor',true );
			$labora_sh_padding   		= get_post_meta( $postid,'labora_sh_padding',true );
			$labora_sh_textcolor_val 	= $labora_sh_txt_clr ? 'color:' . $labora_sh_txt_clr . ';':'';
			$labora_sh_padding_val 	= $labora_sh_padding ? 'padding:' . $labora_sh_padding . ';':'';

			// conditional execution of subheader
			if (
				is_page() || // if is page
				is_page_template() || // if is page template
				( is_single() ) ||  // if is single page
				( is_front_page() && $postid != null ) ||  // if is not empty static frontpage
				( is_home() && $postid != null ) // if is not empty static frontpage
				) {
				// subheader background properties
				if ( is_array( $labora_sh_bg_properties ) && ! empty( $labora_sh_bg_properties['0']['image'] ) ) {
					$labora_subheader_props = 'background:url(' . $labora_sh_bg_properties['0']['image'] . ') ' . $labora_sh_bg_properties['0']['position'] . ' ' . $labora_sh_bg_properties['0']['repeat'] . ' ' . $labora_sh_bg_properties['0']['attachement'] . ' ' . $labora_sh_bg_properties['0']['color'] . ';';
				} elseif ( is_array( $labora_sh_bg_properties ) && ! empty( $labora_sh_bg_properties['0']['color'] ) ) {
					$labora_subheader_props = 'background-color:' . $labora_sh_bg_properties['0']['color'] . ';';
				} elseif ( ! is_array( $labora_sh_bg_properties ) && $labora_sh_bg_properties != '' ) {
					$labora_subheader_props  = 'background:url(' . $labora_sh_bg_properties . ' );';
				}

				$labora_sh_bg_css = ( $labora_subheader_props != '' ) ? ' style="' . $labora_subheader_props . '"' : '';
				$labora_sh_css = ( $labora_sh_textcolor_val != '' || $labora_sh_padding_val != ''  ) ? ' style="' . $labora_sh_textcolor_val . $labora_sh_padding_val . '"' : '';

				// Subheader Option
				switch ( $labora_sub_option ) {
					case 'customtitle':
						$labora_custom_title = get_post_meta( $postid, 'labora_page_title', true );
						if ( $labora_custom_title ) {
							$labora_sub_title = get_post_meta( $postid, 'labora_page_title', true );
						} else {
							$labora_sub_title = $labora_page->post_title;
						}

						$labora_sub_desc = stripslashes( do_shortcode( get_post_meta( $postid, 'labora_page_desc', true ) ) );
						break;
					case 'default':
						if ( get_option( 'labora_teaser' ) == 'default' ) :
							$labora_sub_title = $labora_page->post_title;
						elseif ( get_option( 'labora_teaser' ) == 'disable' ) :
						else :
							$labora_sub_title = $labora_page->post_title;
						endif;
						break;
					default:
						if ( get_option( 'labora_teaser' ) == 'default' ) :
							$labora_sub_title = $labora_page->post_title;
						elseif ( get_option( 'labora_teaser' ) == 'disable' ) :
						else :
							$labora_sub_title = $labora_page->post_title;
						endif;
						break;
				}
			}

			// iF IS  is_single
			if ( class_exists( 'woocommerce' ) ) {
				if ( is_product() && is_woocommerce() ) {
					$labora_sub_title = $labora_page->post_title;
				}
			} 
			if ( is_single() ) {
				if ( $labora_sub_option == 'customtitle' ) {
					$labora_sub_title = get_post_meta( $postid, 'labora_page_title', true );
				} else {
					$labora_sub_title = $labora_page->post_title;
				}
			}
			
			//
			if (  is_singular( 'slider' )
				|| is_singular( 'testimonialtype' )
				|| is_singular( 'cases' )
				|| is_singular( 'staff' )
				|| is_singular( 'gallery' )
				|| is_singular( 'vacancy' )
				|| is_singular( 'service' )
				) {
				if ( $labora_sub_option == 'customtitle' ) {
					$labora_sub_title = get_post_meta( $postid, 'labora_page_title', true );
				} else {
					$labora_sub_title = $labora_page->post_title;
				}
			}

			// If is static blog Page
			if ( is_home() ) {
				if ( $labora_sub_option == 'customtitle' ) {
					//$labora_sub_title = get_post_meta( $postid, 'labora_page_title', true );
				} else {
					$labora_sub_title = esc_html__( 'News & Events', 'labora' );
				}
			}

			// If is product page in WooCommerce
			if ( is_singular( 'product' ) ) {
				$labora_sub_title = esc_html__( 'Shop', 'labora' );
			}

			// Is archive
			if ( is_archive() ) {
				if ( class_exists( 'woocommerce' ) ) {
					if ( is_shop() ) {
						if ( $labora_sub_option == 'customtitle' ) {
							$labora_sub_title = get_post_meta( $postid, 'labora_page_title', true );
						} else {
							$labora_sub_title = esc_html__( 'Shop', 'labora' );
						}
					}
				} else {
					$labora_sub_title = esc_html__( 'Archives', 'labora' );
				}
				//
				if ( is_category() ) {

					$labora_taxonomy_archive_query_obj = $wp_query->get_queried_object();
					$labora_cat_title = $labora_taxonomy_archive_query_obj->name; // Taxonomy term name
					$labora_sub_title = sprintf( esc_html__( 'Category Archives: %s', 'labora' ), '<span>' . $labora_cat_title . '</span>' );
				}
				//
				if ( is_tax( 'gallery_category' ) ) {
					$labora_taxonomy_archive_query_obj = $wp_query->get_queried_object();
					$labora_cat_title = $labora_taxonomy_archive_query_obj->name; // Taxonomy term name
					$labora_sub_title = $labora_cat_title;
				}

				//
				if ( is_day() ) :
						$labora_sub_title = sprintf( esc_html__( 'Archive for date: %s', 'labora' ), '<span>' . get_the_date() . '</span>' );
					elseif ( is_month() ) :
						$labora_sub_title = sprintf( esc_html__( 'Archive for month: %s', 'labora' ), '<span>' . get_the_date( 'F Y' ) . '</span>' );
					elseif ( is_year() ) :
						$labora_sub_title = sprintf( esc_html__( 'Archive for year: %s', 'labora' ), '<span>' . get_the_date( 'Y' ) . '</span>' );
					else :
				endif;
			}

			// If is tag page
			if ( is_tag() ) {
				$labora_sub_title = sprintf( esc_html__( 'Tag Archives: %s', 'labora' ), '<span>' . single_tag_title( '', false ) . '</span>' );
			}
			// If is Search
			if ( is_search() ) {
				$labora_sub_title = sprintf( esc_html__( 'Search Results : %s', 'labora' ), stripslashes( strip_tags( get_search_query() ) ) );
			}
			// If is author
			if ( is_author() ) {
				$labora_cur_auth = ( get_query_var( 'author_name' ) ) ? get_user_by( 'slug', get_query_var( 'author_name' ) ) : get_userdata( get_query_var( 'author' ) );
				$labora_sub_title = sprintf( esc_html__( 'Author Archives: %s', 'labora' ), $labora_cur_auth->display_name );
			}
			/**
			 * Subheader content alignment
			 * gets an default case from theme options panel
			 * gets an alignment for specific page
			 */
			if ( $labora_pagetitle_styling == '' ) {
				$labora_pagetitle_styling = get_option( 'labora_subheader_style' ) ? get_option( 'labora_subheader_style' ) : 'left';
			}
			switch ( $labora_pagetitle_styling ) {
				case 'left':
							$labora_subtitle_align = 'sleft';
							break;
				case 'right':
							$labora_subtitle_align = 'sright';
							break;
				case 'center':
							$labora_subtitle_align = 'scenter';
							break;
				default :
							$labora_subtitle_align = 'sleft';
			}
			if ( get_option( 'labora_teaser' ) != 'disable' ) {

				// If is not disabled from page
				if ( $labora_sub_option != 'disable' ) {

					// Header style4 style
					$labora_h4_id = $sub_header_txtcolor = '';
					$headerstyle = get_option( 'labora_headerstyle' );

					if ( isset( $headerstyle ) && $headerstyle == 'headerstyle4' ) {
						if ( $labora_sh_textcolor_val != '' ) {
							$sub_header_txtcolor = $labora_sh_textcolor_val;
						}
						$labora_h4_id = 'style="' . $sub_header_txtcolor . ' ' . $labora_sh_padding_val . ' "';
						$labora_sh_css = '';
					}
					$labora_sh_text_css = ( $labora_sh_textcolor_val != '' ) ? ' style="' . $labora_sh_textcolor_val . '"' : '';
					$out .= '<div id="subheader" ' . $labora_h4_id . ' class="' . $labora_subtitle_align . '" ' . $labora_sh_css . '>';

					if ( ! empty( $labora_sh_bg_css ) ) {
						if ( $headerstyle == 'headerstyle1' || $headerstyle == 'headerstyle2' ||  $headerstyle == 'headerstyle3' ||  $headerstyle == 'fixedheader' ) {
							$out .= '<div class="subheader_bg_image"></div>';
							if( $labora_sh_bg_properties['0']['image'] ) {
								$out .= '<div class="subheader_bg_overlay"></div>';
							}
						}
					}
					$out .= '<div class="subheader-inner">';
					$out .= '<div class="subdesc">';
					if ( $labora_sub_title ) {
						$out .= '<h1 class="page-title" ' . $labora_sh_text_css . '>' . $labora_sub_title . '</h1>';
					}
					if ( $labora_sub_desc ) {
						$out .= '<div class="customtext">' . $labora_sub_desc . '</div>';
					}
					$out .= '</div>';
					$out .= $this->labora_breadcrumb( $postid );
					ob_start();
					$out .= ob_get_contents();
					ob_end_clean();
					$out .= '</div>';
					$out .= '</div>';
				}
			}

			$allowed_tags = wp_kses_allowed_html( 'post' );
			echo  wp_kses( $out, $allowed_tags );
		}


		// B R E A D C R U M B S
		//--------------------------------------------------------
		function labora_breadcrumb( $postid ) {

			$labora_sh_breadcrumb 	= get_post_meta( $postid, 'labora_breadcrumb', true );
			$bc_out = '';
			if ( function_exists( 'bcn_display' ) ) {
				if ( is_tag() || is_search() || is_404() || is_author() || is_archive() ) {
					if ( get_option( 'labora_breadcrumbs' ) != 'on' ) {
						$bc_out .= '<div class="breadcrumb-main"><div class="breadcrumb-wrap inner">';
						$bc_out .= '<div class="breadcrumbs">';
						ob_start();
						$bc_out .= bcn_display();
						$bc_out .= ob_get_contents();
						ob_end_clean();
						$bc_out .= '</div>';
						$bc_out .= '</div></div>';
					}
				} else {
					if ( $labora_sh_breadcrumb != 'on' and ( get_option( 'labora_breadcrumbs' ) != 'on' ) and ! is_front_page() ) {
						$bc_out .= '<div class="breadcrumb-main"><div class="breadcrumb-wrap inner">';
						$bc_out .= '<div class="breadcrumbs">';
						ob_start();
						$bc_out .= bcn_display();
						$bc_out .= ob_get_contents();
						ob_end_clean();
						$bc_out .= '</div>';
						$bc_out .= '</div></div>';
					}
				}
			}
			return $bc_out;
		}

		// A B O U T   A U T H O R
		//--------------------------------------------------------
		function labora_about_author() {
			if ( get_the_author_meta( 'description' ) != '' ) {?>
			<div id="about-author">
				<div class="author_containter">
					<div class="author-avatar"><?php echo get_avatar( get_the_author_meta( 'email' ), $size = '80', $default = '' ); ?></div>
					<div class="author-description">
					<h4><?php the_author_meta( 'display_name' ); ?></h4>
					<p><?php the_author_meta( 'description' ); ?></p></div>
				</div>
			</div>
		<?php
			}
		}

		// R E L A T E D   P O S T S
		//--------------------------------------------------------
		function labora_related_posts( $postid ) {

			global $wpdb, $post;

			$labora_post_tags = wp_get_post_tags( $postid );
			if ( $labora_post_tags ) {

				$labora_tag_ids = array();
				foreach ( $labora_post_tags as $individual_tag ) {
					$labora_tag_ids[] = $individual_tag->term_id;
				}

				$labora_post_args = array(
					'tag__in'				=> $labora_tag_ids,
					'post__not_in'			=> array( $post->ID ),
					'showposts'				=> 4, // Number of related posts that will be shown.
					'ignore_sticky_posts'	=> 1,
				);

				$related_post_found = 'true';
				$labora_post_query = new wp_query( $labora_post_args );

				if ( $labora_post_query->have_posts() ) {
					echo '<div class="iva-related-posts"><div class="fancyheading textleft"><h4><span>' . esc_html__( 'You might also like', 'labora' ) . '</span></h4></div><ul>';
					while ( $labora_post_query->have_posts() ) {
						$labora_post_query->the_post();
						echo '<li>';
						echo '<a href="' . esc_url( get_permalink( $post->ID ) ) . '">' . get_the_title() . '</a> - <span class="related-date">' . get_the_date() . '</span>';
						echo '</li>';
					}
					echo '</ul>';
					echo '</div>';
				}
			}
			wp_reset_postdata();
		}
	}
	// end class
}

/**
* Description Walker Class for Custom Menu
*/
//http://code.tutsplus.com/tutorials/understanding-the-walker-class--wp-25401
class Labora_Custom_Menu_Walker extends Walker_Nav_Menu {

	// columns
	var $columns  = 0;
	var $max_columns = 0;
	// rows
	var $rows   = 1;
	var $arows   = array();
	// mega menu
	var $labora_megamenu = 0;

	function start_lvl( &$output, $depth = 0, $args = array() ) {

		$indent = str_repeat( "\t", $depth );

		if ( $depth == 0 && $this->iva_megamenu ) {
			 $output .= "\n$indent<div class=\"sf-mega\"><div class=\"sf-mega-wrap\">\n";
		} else {
			$output .= "\n$indent<ul>\n";
		}
	}


	function end_lvl( &$output, $depth = 0, $args = array() ) {

		$indent = str_repeat( "\t", $depth );

		if ( $depth == 0 && $this->iva_megamenu ) {
			$output .= "$indent</div></div>\n";
		} else {
			$output .= "$indent</ul>\n";
		}

		if ( $depth == 0 ) {

			if ( $this->iva_megamenu ) {
				$output = str_replace( '{menu_ul_class}', ' sf-mega-section mmcol-' . $this->max_columns, $output );
				foreach ( $this->arows as $row => $columns ) {
					$output = str_replace( '{menu_li_class_' . $row . '}', 'sf-mega-section mmcol-' . $columns, $output );
				}

				$this->columns		= 0;
				$this->max_columns	= 0;
				$this->arows		= array();
			} else {
				$output = str_replace( '{menu_ul_class}', '', $output );
			}
		}
	}

	function start_el( &$output, $object, $depth = 0, $args = array(), $current_object_id = 0 ) {

		global $wp_query;

		$object_output = $column_class = $li_text_block_class = '';

		$classes = empty( $object->classes ) ? array() : (array) $object->classes;

		$class = array();

		if ( $depth == '0' ) {
			$this->iva_megamenu	= get_post_meta( $object->ID, 'menu-item-iva-megamenu', true );
			if ( $this->iva_megamenu == 'on' ) { $li_text_block_class = 'iva-megamenu  '; }
		}

		if ( $depth == 1 && $this->iva_megamenu ) {

			$this->columns ++;
			$this->arows[ $this->rows ] = $this->columns;

			if ( $this->max_columns < $this->columns ) {$this->max_columns = $this->columns;}

			$attributes  = ! empty( $object->attr_title ) ? ' title="' . esc_attr( $object->attr_title ) . '"' : '';
			$attributes .= ! empty( $object->target )     ? ' target="' . esc_attr( $object->target ) . '"' : '';
			$attributes .= ! empty( $object->xfn )        ? ' rel="' . esc_attr( $object->xfn ) . '"' : '';
			$attributes .= ! empty( $object->url )        ? ' href="' . esc_attr( $object->url ) . '"' : '';

			$prepend = $append = $description = '';

			$description  = ! empty( $object->attr_title ) ? '<span class="msubtitle">' . esc_attr( $object->attr_title ) . '</span>' : '';

			if ( $depth != 0 ) {
				$description = $append = $prepend = '';
			}
			$object_output = $args->before;
			$object_output .= '<a' . $attributes . ' class="col_title">';
			if ( $classes['0'] != '' ) {
				$object_output .= '<i class= "iva_menu_icon fa fa-caret-right ' . $classes['0'] . '"></i>';
			}
			$object_output .= $args->link_before . $prepend . apply_filters( 'the_title', $object->title, $object->ID ) . $append;
			$object_output .= $description . $args->link_after;
			$object_output .= '</a>';
			$object_output .= $args->after;
			$column_class = ' {menu_li_class_' . $this->rows . '}';
		} else {
			$attributes  = ! empty( $object->attr_title ) ? ' title="' . esc_attr( $object->attr_title ) . '"' : '';
			$attributes .= ! empty( $object->target )     ? ' target="' . esc_attr( $object->target ) . '"' : '';
			$attributes .= ! empty( $object->xfn )        ? ' rel="' . esc_attr( $object->xfn ) . '"' : '';
			$attributes .= ! empty( $object->url )        ? ' href="' . esc_attr( $object->url ) . '"' : '';

			$prepend = $append = $description = '';

			$description  = ! empty( $object->attr_title ) ? '<span class="msubtitle">' . esc_attr( $object->attr_title ) . '</span>' : '';

			if ( $depth != 0 ) {
				 $description = $append = $prepend = '';
			}
			$object_output = $args->before;
			$object_output .= '<a ' . $attributes . '>';
			if ( $classes['0'] != '' ) {
				$object_output .= '<i class="iva_menu_icon fa fa-caret-right ' . $classes['0'] . '"></i>';
			}
			$object_output .= $args->link_before . $prepend . apply_filters( 'the_title', $object->title, $object->ID ) . $append;
			$object_output .= $description . $args->link_after;
			$object_output .= '</a>';
			$object_output .= $args->after;
		}

		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
		$class_names = $value = '';
		$class_names = apply_filters( 'nav_menu_css_class', array_filter( $classes ) );

		foreach ( $class_names as $key => $values ) {
			if ( $key != '0' ) {
				$class[] .= $values;
			}
		}
		$class_names = join( ' ',$class );
		$class_names = ' class="' . $li_text_block_class . esc_attr( $class_names ) . $column_class . '"';

		if ( $depth == 1 && $this->iva_megamenu ) {
			$output .= $indent . '<div id="menu-item-' . $object->ID . '"' . $value . $class_names . '>';
		} else {
			$output .= $indent . '<li id="menu-item-' . $object->ID . '"' . $value . $class_names . '>';
		}
		$output .= apply_filters( 'walker_nav_menu_start_el', $object_output, $object, $depth, $args );
	}

	function end_el( &$output, $object, $depth = 0, $args = array() ) {

		$indent = str_repeat( "\t", $depth );
		if ( $depth == 1 && $this->iva_megamenu ) {
		 	$output .= "$indent</div>\n";
		} else {
			$output .= "$indent</li>\n";
		}
	}
}


/**
 * Description Walker Class for Mobile Menu
 */
class Labora_Mobile_Custom_Menu_Walker extends Walker_Nav_Menu {
	function start_el( &$output, $object, $depth = 0, $args = array(), $current_object_id = 0 ) {

		global $wp_query;

		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$class_names = $value = '';

		$classes = empty( $object->classes ) ? array() : (array) $object->classes;
		$class = array();
		$class_names = apply_filters( 'nav_menu_css_class', array_filter( $classes ) );

		foreach ( $class_names as $key => $values ) {
			if ( $key != '0' ) {
				$class[] .= $values;
			}
		}
		$custommneu_class = join( ' ',$class );
		$class_menu = ' class="' . esc_attr( $custommneu_class ) . '"';
		$output .= $indent . '<li id="mobile-menu-item-' . $object->ID . '"' . $value . $class_menu . '>';

		$attributes  = ! empty( $object->attr_title ) ? ' title="' . esc_attr( $object->attr_title ) . '"' : '';
		$attributes .= ! empty( $object->target )     ? ' target="' . esc_attr( $object->target ) . '"' : '';
		$attributes .= ! empty( $object->xfn )        ? ' rel="' . esc_attr( $object->xfn ) . '"' : '';
		$attributes .= ! empty( $object->url )        ? ' href="' . esc_attr( $object->url ) . '"' : '';

		$prepend = $append = '';

		$description  = ! empty( $object->attr_title ) ? '<span class="msubtitle">' . esc_attr( $object->attr_title ) . '</span>' : '';

		if ( $depth != 0 ) {
			 $description = $append = $prepend = '';
		}

		$object_output = $args->before;
		$object_output .= '<a' . $attributes . '>';

		if ( $classes['0'] != '' ) {
			$object_output .= '<i class="iva_menuicon fa ' . $classes['0'] . ' fa-lg"></i>';
		}

		$object_output .= $args->link_before . $prepend . apply_filters( 'the_title', $object->title, $object->ID ) . $append;
		$object_output .= $description . $args->link_after;

		if ( 'primary-menu' == $args->theme_location ) {
			$submenus = 0 == $depth || 1 == $depth ? get_posts( array( 'post_type' => 'nav_menu_item', 'numberposts' => 1, 'meta_query' => array( array( 'key' => '_menu_item_menu_item_parent', 'value' => $object->ID, 'fields' => 'ids' ) ) ) ) : false;
			$object_output .= ! empty( $submenus ) ? ( 0 == $depth ? '<span class="iva-children-indenter"><i class="fa fa fa-angle-down"></i></span>' : '<span class="iva-children-indenter"><i class="fa fa fa-angle-down"></i></span>' ) : '';
		}

		$object_output .= '</a>';
		$object_output .= $args->after;

		$output .= apply_filters( 'walker_nav_menu_start_el', $object_output, $object, $depth, $args );
	}
}
// W R I T E   G E N E R A T O R
//--------------------------------------------------------

/**
 * function labora_generator()
 * link @ http://php.net/manual/en/function.call-user-func-array.php
 * link @ http://php.net/manual/en/function.func-get-args.php
 */
function labora_generator( $function ) {

	global $labora_generator;

	$labora_generator 	= new Labora_Generator_Class;
	$labora_args 		= array_slice( func_get_args(), 1 );

	return call_user_func_array( array( &$labora_generator, $function ), $labora_args );
}

// C U S T O M   C O M M E N T   T E M P L A T E
//--------------------------------------------------------

function labora_custom_comment( $comment, $args, $depth ) {

	global $post;

	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
				?>
				<li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
					<p><?php esc_html_e( 'Pingback:', 'labora' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( esc_html__( 'Edit', 'labora' ), '<span class="edit-link">', '</span>' ); ?></p>
						<?php
						break;
		default :
			// Proceed with normal comments.
			?>
			<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
			<div id="comment-<?php comment_ID(); ?>" class="comment-body">
				<header class="comment-meta comment-author vcard">
					<?php
						echo get_avatar( $comment, 60 );
						printf( '<cite class="fn">%1$s %2$s</cite>',
							get_comment_author_link(),
							// If current post author is also comment author, make it known visually.
							( $comment->user_id === $post->post_author ) ? '<span> ' . esc_html__( 'Post author', 'labora' ) . '</span>' : ''
						);
						echo '<div class="comment-metadata">';
						printf( '<a href="%1$s"><time pubdate datetime="%2$s">%3$s</time></a>',
							esc_url( get_comment_link( $comment->comment_ID ) ),
							get_comment_time( 'c' ),
							/* translators: 1: date, 2: time */
							sprintf( esc_html__( '%1$s at %2$s', 'labora' ), get_comment_date(), get_comment_time() )
						);

						edit_comment_link( esc_html__( 'Edit', 'labora' ), '<span class="edit-link">', '</span>' );
						echo '</div>';
					?>
				</header><!-- .comment-meta -->
				<div class="comment-content">
					<?php if ( '0' == $comment->comment_approved ) : ?>
					<p class="comment-awaiting-moderation"><?php esc_html_e( 'Your comment is awaiting moderation.', 'labora' ); ?></p>
					<?php endif; ?>

					<?php comment_text(); ?>

					<div class="reply">
						<?php comment_reply_link( array_merge( $args, array( 'reply_text' => esc_html__( 'Reply', 'labora' ), 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
					</div><!-- .reply -->
				</div><!-- .comment-content -->
			</div><!-- #comment-## -->
			<?php
			break;
	endswitch; // end comment_type check
}
